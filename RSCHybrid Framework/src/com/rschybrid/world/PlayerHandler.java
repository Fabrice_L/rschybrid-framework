package com.rschybrid.world;

import java.util.ArrayList;

import com.rschybrid.model.player.Player;
import com.rschybrid.net.packet.outgoing.PlayerPositionPacket;
import com.rschybrid.net.packet.outgoing.PlayerUpdatePacket;

public class PlayerHandler {
	
	private static ArrayList<Player> playerSessions = new ArrayList<>();
	private static ArrayList<Player> playersOnline = new ArrayList<>();

	public static ArrayList<Player> getPlayerSessions() {
		return playerSessions;
	}

	public static ArrayList<Player> getPlayersOnline() {
		return playersOnline;
	}
	
	public static int getFreeIndex() {
		int id = 0;
		for(Player player : playersOnline) {
			if (id != player.getId()) {
				return id;
			}
			id++;
		}
		return id;
	}

	public static boolean isOnline(String username) {
		for (Player player : playersOnline) {
			if(player.getName().equalsIgnoreCase(username)) {
				return true;
			}
		}
		return false;
	}
	
	public static void updatePlayer(Player player) {
    	//Updating viewable lists
    	player.updateViewedPlayers();
    	
    	//chat
    	player.updateMessageQueues();
    	
    	//Walking
		player.setHasMoved(false);
    	player.getPathHandler().updatePosition();
    	player.updateAppearanceID();
    	
    	//Sending update packets
    	player.send(new PlayerPositionPacket());
    	player.send(new PlayerUpdatePacket());
    	
    	//Updating the entire collection
		player.getWatchedPlayers().update();
		
		player.setOurAppearanceChanged(false);
    	player.getChatMessagesNeedingDisplayed().clear();
	}
		
}

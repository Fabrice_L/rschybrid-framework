package com.rschybrid.world;

import java.io.BufferedInputStream;
import java.io.File;
import java.nio.ByteBuffer;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;

import com.rschybrid.model.landscape.MutableTileValue;
import com.rschybrid.model.landscape.Sector;
import com.rschybrid.model.landscape.TileValue;
import com.rschybrid.model.landscape.def.TileDef;
import com.rschybrid.model.object.def.DoorDef;
import com.rschybrid.util.Misc;

public class LandscapeHandler {
	
	protected static final Logger logger = Logger.getLogger(LandscapeHandler.class.getName());
	
	public static final int MAX_WIDTH = 944;
	public static final int MAX_HEIGHT = 3776;
	private static TileValue[][] tileType = new TileValue[MAX_WIDTH][MAX_HEIGHT];
	
	private static ZipFile tileArchive;

	public static boolean withinWorld(int x, int y) {
		return x >= 0 && x < MAX_WIDTH && y >= 0 && y < MAX_HEIGHT;
	}

	public static TileValue getTileValue(int x, int y) {
		if (!withinWorld(x, y)) {
			return null;
		}
		TileValue t = tileType[x][y];
		if (t == null) {
			t = TileValue.create(0, new byte[6]);
			tileType[x][y] = t;
		}
		return t;
	}
	
	public static void load() {
		try {
			tileArchive = new ZipFile(new File("data/Landscape.rscd"));
		}
		catch(Exception e) {
		}
		int i = 0;
		for(int lvl = 0;lvl < 4;lvl++) {
			int wildX = 2304;
			int wildY = 1776 - (lvl * 944);
			for(int sx = 0;sx < 1000;sx += 48) {
				for(int sy = 0;sy < 1000;sy += 48) {
					int x = (sx + wildX) / 48;
					int y = (sy + (lvl * 944) + wildY) / 48;
					i++;
					loadSection(x, y, lvl, sx, sy + (944 * lvl));
				}
			}
		}
		logger.log(Level.INFO, "Loaded " + i + " landscape sections.");
	}
	
	public static void setTileValue(int x, int y, TileValue tileValue) {
		tileType[x][y] = tileValue;
	}
	
	private static boolean loadSection(int sectionX, int sectionY, int height, int bigX, int bigY) {
		// Logging.debug(1);
		Sector s = null;
		try {
			String filename = "h" + height + "x" + sectionX + "y" + sectionY;
			ZipEntry e = tileArchive.getEntry(filename);
			if (e == null) {
				// throw new Exception("Missing tile: " + filename);
				System.out.println("Missing tile: " + filename + " ex: "
						+ (bigX + 10) + ", " + (bigY + 10));
				return false;
			}
			ByteBuffer data = Misc
					.streamToBuffer(new BufferedInputStream(tileArchive
							.getInputStream(e)));
			s = Sector.unpack(data);
			// s = modifyAndSave(filename, s, bigX, bigY);
		} catch (Exception e) {
			e.printStackTrace();
		}
		// Logging.debug(2);
		for (int y = 0; y < Sector.HEIGHT; y++) {
			for (int x = 0; x < Sector.WIDTH; x++) {
				int bx = bigX + x;
				int by = bigY + y;
				if (!withinWorld(bx, by)) {
					continue;
				}
				MutableTileValue t = new MutableTileValue(getTileValue(
						bx, by));
				t.overlay = s.getTile(x, y).groundOverlay;
				t.diagWallVal = s.getTile(x, y).diagonalWalls;
				t.horizontalWallVal = s.getTile(x, y).horizontalWall;
				t.verticalWallVal = s.getTile(x, y).verticalWall;
				t.elevation = s.getTile(x, y).groundElevation;
				/** start of shit **/
				if ((s.getTile(x, y).groundOverlay & 0xff) == 250) {
					s.getTile(x, y).groundOverlay = (byte) 2;
				}
				/** break in shit **/
				int groundOverlay = s.getTile(x, y).groundOverlay & 0xFF;
				if (groundOverlay > 0
						&& TileDef.getDef(groundOverlay - 1)
								.getObjectType() != 0) {
					t.mapValue |= 0x40; // 64
				}

				int verticalWall = s.getTile(x, y).verticalWall & 0xFF;
				if (verticalWall > 0
						&& DoorDef.getDef(verticalWall - 1)
								.getUnknown() == 0
						&& DoorDef.getDef(verticalWall - 1)
								.getDoorType() != 0) {
					t.mapValue |= 1; // 1
					MutableTileValue t1 = new MutableTileValue(
							getTileValue(bx, by - 1));
					t1.mapValue |= 4; // 4
					setTileValue(bx, by - 1, t1.toTileValue());
				}

				int horizontalWall = s.getTile(x, y).horizontalWall & 0xFF;
				if (horizontalWall > 0
						&& DoorDef.getDef(horizontalWall - 1)
								.getUnknown() == 0
						&& DoorDef.getDef(horizontalWall - 1)
								.getDoorType() != 0) {
					t.mapValue |= 2; // 2
					MutableTileValue t1 = new MutableTileValue(
							getTileValue(bx - 1, by));
					t1.mapValue |= 8;
					setTileValue(bx - 1, by, t1.toTileValue());
				}

				int diagonalWalls = s.getTile(x, y).diagonalWalls;
				if (diagonalWalls > 0
						&& diagonalWalls < 12000
						&& DoorDef.getDef(diagonalWalls - 1)
								.getUnknown() == 0
						&& DoorDef.getDef(diagonalWalls - 1)
								.getDoorType() != 0) {
					t.mapValue |= 0x20; // 32
				}
				if (diagonalWalls > 12000
						&& diagonalWalls < 24000
						&& DoorDef.getDef(diagonalWalls - 12001)
								.getUnknown() == 0
						&& DoorDef.getDef(diagonalWalls - 12001)
								.getDoorType() != 0) {
					t.mapValue |= 0x10; // 16
				}
				setTileValue(bx, by, t.toTileValue());
			}
		}
		return true;
	}
}

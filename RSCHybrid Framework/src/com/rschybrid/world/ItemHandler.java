package com.rschybrid.world;

import java.util.ArrayList;

import com.rschybrid.model.Point;
import com.rschybrid.model.item.GameItem;
import com.rschybrid.model.item.GroundItem;
import com.rschybrid.model.item.GroundItem.ItemState;
import com.rschybrid.model.player.Player;
import com.rschybrid.net.packet.outgoing.ItemPositionPacket;

public class ItemHandler {
	
	private static ArrayList<GroundItem> groundItems = new ArrayList<GroundItem>();

	public static ArrayList<GroundItem> getGroundItems() {
		return groundItems;
	}
	
	
	public static void updateItem(Player player) {
		player.updateViewedItems();
		player.send(new ItemPositionPacket());
		player.getWatchedItems().update();
		for (int i = 0; i < groundItems.size(); i++) {
			GroundItem item = groundItems.get(i);
			if (item.getState() == ItemState.PRIVATE) {
				if (item.getPrivateTime() > 0) {
					item.decreasePrivateTime();
				} else {
					item.setState(ItemState.PUBLIC);
				}
			}
			else if (item.getState() == ItemState.PUBLIC) {
				if (item.getPublicTime() > 0) {
					item.decreasePublicTime();
				} else {
					groundItems.remove(item);
				}
			}
		}
	}
	
	public static void dropItem(GameItem item, Point location, Player owner) {
		GroundItem groundItem = new GroundItem(item, owner, location);
		if (owner != null) {
			groundItem.setState(ItemState.PRIVATE);
		}
		for (int i = 0; i < groundItems.size(); i++) {
			if (groundItems.get(i).visibleTo(owner) 
					&& groundItems.get(i).canMerge(groundItem) 
					&& groundItems.get(i).getLocation().equals(location)
					&& groundItems.get(i).getId() == groundItem.getId()) {
				groundItem.setAmount(groundItems.get(i).getAmount() + groundItem.getAmount());
				groundItems.remove(i);
			}
		}
		System.out.println(item.getAmount());
		groundItems.add(groundItem);
	}

}

package com.rschybrid.net.codec;

import java.util.List;

import com.rschybrid.net.packet.Packet;

import io.netty.buffer.ByteBuf;
import io.netty.buffer.Unpooled;
import io.netty.channel.ChannelHandlerContext;
import io.netty.handler.codec.ByteToMessageDecoder;

public final class Decoder extends ByteToMessageDecoder {

    @Override
    public void decode(ChannelHandlerContext ctx, ByteBuf buffer, List<Object> out)
            throws Exception {
        buffer.markReaderIndex();
        if (buffer.readableBytes() < 2) {
            return;
        }

        boolean shortLen = false;
        int length = buffer.readByte();
        if (length >= 160) {
            length = (length - 160) * 256 + buffer.readByte();
            shortLen = true;
        }

        // If the remaining payload has less than length bytes then
        // the packet hasn't fully arrived. Rewind the buffer and return.
        if (length > buffer.readableBytes()) {
            buffer.resetReaderIndex();
            return;
        }

        // If the length is a short, or the packet has no payload, then we can just
        // read the contents of buffer.
        // However, if the length is a byte then the next byte is actually the last
        // value of the payload, so we need to put it back in its correct position.
        int opcode;
        ByteBuf payload;
        if (shortLen || length <= 1) {
            opcode = buffer.readUnsignedByte();
            payload = buffer;
        } else {
            byte[] data = new byte[length - 1];
            data[length - 2] = buffer.readByte();
            opcode = buffer.readUnsignedByte();
            buffer.readBytes(data, 0, length - 2);
            payload = Unpooled.wrappedBuffer(data);
        }
        
        --length;

        out.add(new Packet(ctx.channel(), opcode, payload));
    }
}
package com.rschybrid.net;

import java.util.Queue;
import java.util.concurrent.ConcurrentLinkedQueue;

import com.rschybrid.model.player.Player;
import com.rschybrid.net.packet.IncomingPacket;
import com.rschybrid.net.packet.Packet;

public class PacketManager {
	
	private Player player;
	private Queue<Packet> queuedPackets = new ConcurrentLinkedQueue<Packet>();
	
	public void queuePacket(Packet packet) {
		queuedPackets.add(packet);
	}
	
	public PacketManager(Player player) {
		this.player =  player;
	}

	@SuppressWarnings("unlikely-arg-type")
	public boolean processQueuedPackets() {
		Packet packets = null;
		while((packets = queuedPackets.poll()) != null) {
			IncomingPacket incomingPacket = IncomingPacket.getPackets()[packets.getOpcode()];
			if (incomingPacket != null) {
				try {
					player.send(incomingPacket, packets.getPayload());
				} catch (Exception e) {
					e.printStackTrace();
				}
			} else {
				System.out.println("Unhandled Incoming Packet ID: " + packets.getOpcode());
			}
			queuedPackets.remove(incomingPacket);
		}
		return true;
	}

}

package com.rschybrid.net.packet.outgoing;

import com.rschybrid.model.player.GameAction;
import com.rschybrid.model.player.Player;
import com.rschybrid.net.packet.OutgoingPacket;
import com.rschybrid.net.packet.PacketBuilder;

public class CloseBankPacket extends OutgoingPacket {

	@Override
	public void send(Player player) {
		player.setGameAction(GameAction.NOTHING);
		PacketBuilder s = new PacketBuilder();
		s.setId(171);
		player.getChannel().writeAndFlush(s.toPacket());
	}

}

package com.rschybrid.net.packet.outgoing;

import java.util.ArrayList;

import com.rschybrid.model.item.GameItem;
import com.rschybrid.model.player.Player;
import com.rschybrid.net.packet.OutgoingPacket;
import com.rschybrid.net.packet.PacketBuilder;
import com.rschybrid.util.Misc;

public class ShowTradeAcceptPacket extends OutgoingPacket {

	@Override
	public void send(Player player) {
		Player with = player.getTrade().getTradingWith();
		if(with == null) { // This shouldn't happen
			return;
		}
      	PacketBuilder s = new PacketBuilder();
      	s.setId(251);
      	s.writeLong(Misc.usernameToHash(with.getName()));
      	ArrayList<GameItem> uniqueItemsInTrade = new ArrayList<GameItem>();
      	for(GameItem item : with.getTradeContainer().getItems()) {
      		boolean canAdd = true;
          	for (GameItem items : uniqueItemsInTrade) {
          		if (items.getId() == item.getId()) {
          			canAdd = false;
          		}
          	}
          	if (canAdd) {
          		uniqueItemsInTrade.add(item);
          	}
      	}
      	s.writeByte(uniqueItemsInTrade.size());
      	for (GameItem item : uniqueItemsInTrade) {
      		s.writeShort(item.getId());
      		s.writeInt(with.getTradeContainer().getItemAmount(item, item.getDefinitions().isStackable()));
      	}
      	uniqueItemsInTrade.clear();
      	for(GameItem item : player.getTradeContainer().getItems()) {
      		boolean canAdd = true;
          	for (GameItem items : uniqueItemsInTrade) {
          		if (items.getId() == item.getId()) {
          			canAdd = false;
          		}
          	}
          	if (canAdd) {
          		uniqueItemsInTrade.add(item);
          	}
      	}
      	s.writeByte(uniqueItemsInTrade.size());
      	for (GameItem item : uniqueItemsInTrade) {
      		s.writeShort(item.getId());
      		s.writeInt(player.getTradeContainer().getItemAmount(item, item.getDefinitions().isStackable()));
      	}
      	player.getChannel().writeAndFlush(s.toPacket());
	}

}

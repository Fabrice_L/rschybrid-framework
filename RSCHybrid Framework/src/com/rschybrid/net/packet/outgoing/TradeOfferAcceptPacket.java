package com.rschybrid.net.packet.outgoing;

import com.rschybrid.model.player.Player;
import com.rschybrid.net.packet.OutgoingPacket;
import com.rschybrid.net.packet.PacketBuilder;

public class TradeOfferAcceptPacket extends OutgoingPacket {

	@Override
	public void send(Player player) {
		Player with = player.getTrade().getTradingWith();
		if(with == null) { // This shouldn't happen
			return;
		}
	      	PacketBuilder s1 = new PacketBuilder();
	      	s1.setId(18);
	      	s1.writeByte((player.getTrade().isOfferAccepted() ? 1 : 0));
	      	player.getChannel().writeAndFlush(s1.toPacket());
	      	
	      	PacketBuilder s2 = new PacketBuilder();
	      	s2.setId(92);
	      	s2.writeByte((with.getTrade().isOfferAccepted() ? 1 : 0));
	      	player.getChannel().writeAndFlush(s2.toPacket());
	}

}

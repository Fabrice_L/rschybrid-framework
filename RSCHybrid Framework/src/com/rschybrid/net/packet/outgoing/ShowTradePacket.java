package com.rschybrid.net.packet.outgoing;

import com.rschybrid.model.player.GameAction;
import com.rschybrid.model.player.Player;
import com.rschybrid.net.packet.OutgoingPacket;
import com.rschybrid.net.packet.PacketBuilder;

public class ShowTradePacket extends OutgoingPacket {

	@Override
	public void send(Player player) {
		if (player.getGameAction() != GameAction.TRADING
				|| player.getTrade().getTradingWith() == null
				|| player.getTrade().getTradingWith().getGameAction() != GameAction.TRADING) {
			player.getTrade().resetTrade();
			return;
		}
		PacketBuilder s = new PacketBuilder();
		s.setId(4);
		s.writeShort(player.getTrade().getTradingWith().getId()); //change to trading player
		player.getChannel().writeAndFlush(s.toPacket());
	}

}

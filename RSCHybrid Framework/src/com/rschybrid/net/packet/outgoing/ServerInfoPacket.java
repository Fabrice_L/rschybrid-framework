package com.rschybrid.net.packet.outgoing;

import com.rschybrid.model.player.Player;
import com.rschybrid.net.packet.OutgoingPacket;
import com.rschybrid.net.packet.PacketBuilder;

public class ServerInfoPacket extends OutgoingPacket {

	@Override
	public void send(Player player) {
		String location = "Belgium";
        PacketBuilder pb = new PacketBuilder();
        pb.setId(110);
        pb.writeLong(System.currentTimeMillis()); // Start time
        pb.writeBytes(location.getBytes());
        player.getChannel().writeAndFlush(pb.toPacket());
	}

}

package com.rschybrid.net.packet.outgoing;

import com.rschybrid.model.item.GameItem;
import com.rschybrid.model.player.Player;
import com.rschybrid.net.packet.OutgoingPacket;
import com.rschybrid.net.packet.PacketBuilder;

public class UpdateBankItemPacket extends OutgoingPacket {

	private GameItem item;
	
	public UpdateBankItemPacket(GameItem item) {
		this.item = item;
	}
	@Override
	public void send(Player player) {
		PacketBuilder s = new PacketBuilder();
		s.setId(139);
		s.writeByte(player.getBank().getItemSlot(item));
		s.writeShort(item.getId());
		s.writeInt(item.getAmount());
		player.getChannel().writeAndFlush(s.toPacket());
	}

}

package com.rschybrid.net.packet.outgoing;

import java.util.Collection;

import com.rschybrid.model.Entity;
import com.rschybrid.model.player.Player;
import com.rschybrid.net.packet.Bitpacker;
import com.rschybrid.net.packet.OutgoingPacket;
import com.rschybrid.util.StatefulEntityCollection;

public class PlayerPositionPacket extends OutgoingPacket {

	@Override
	public void send(Player player) {
		StatefulEntityCollection<Player> watchedPlayers = player.getWatchedPlayers();
		Collection<Player> newPlayers = watchedPlayers.getNewEntities();
		Collection<Player> knownPlayers = watchedPlayers.getKnownEntities();
        Bitpacker packet = new Bitpacker();
		packet.setId(145);
		packet.addBits(player.getLocation().getX(), 11);
		packet.addBits(player.getLocation().getY(), 13);
		packet.addBits(player.getSprite(), 4);
		packet.addBits(knownPlayers.size(), 8);
		for(Player p : knownPlayers) {
			if(player.getId() != p.getId()) {
				packet.addBits(p.getId(), 16);
				if(watchedPlayers.isRemoving(p)) {
					packet.addBits(1, 1);
					packet.addBits(1, 1);
					packet.addBits(12, 4);
				}
				else if(p.hasMoved()) {
					packet.addBits(1, 1);
					packet.addBits(0, 1);
					packet.addBits(p.getSprite(), 3);
				}
				else if(p.isSpriteChanged()) {
					packet.addBits(1, 1);
					packet.addBits(1, 1);
					packet.addBits(p.getSprite(), 4);
				}
				else {
					packet.addBits(0, 1);
				}
			}
		}
		for(Player p : newPlayers) {
			byte[] offsets = Entity.getPositionOffsets(p.getLocation(), player.getLocation());
			packet.addBits(p.getId(), 16);
			packet.addBits(offsets[0], 5);
			packet.addBits(offsets[1], 5);
			packet.addBits(p.getSprite(), 4);
			packet.addBits(0, 1);
		}
		player.getChannel().writeAndFlush(packet.toPacket());
	}

}

package com.rschybrid.net.packet.outgoing;

import java.util.Collection;

import com.rschybrid.model.item.GroundItem;
import com.rschybrid.model.player.Player;
import com.rschybrid.net.packet.OutgoingPacket;
import com.rschybrid.net.packet.PacketBuilder;
import com.rschybrid.util.Misc;
import com.rschybrid.util.StatefulEntityCollection;

public class ItemPositionPacket extends OutgoingPacket {

	@Override
	public void send(Player player) {
		StatefulEntityCollection<GroundItem> watchedItems = player.getWatchedItems();
		if(watchedItems.changed()) {
			Collection<GroundItem> newItems = watchedItems.getNewEntities();
			Collection<GroundItem> knownItems = watchedItems.getKnownEntities();
			PacketBuilder packet = new PacketBuilder();
			packet.setId(109);
			for(GroundItem i : knownItems) {
				if(watchedItems.isRemoving(i)) {
					byte[] offsets = Misc.getObjectPositionOffsets(i.getLocation(), player.getLocation());
					packet.writeShort(i.getId() + 32768);
					packet.writeByte(offsets[0]);
					packet.writeByte(offsets[1]);
				}
			}
			for(GroundItem i : newItems) {
				byte[] offsets = Misc.getObjectPositionOffsets(i.getLocation(), player.getLocation());
				packet.writeShort(i.getId());
				packet.writeByte(offsets[0]);
				packet.writeByte(offsets[1]);
			}
			player.getChannel().writeAndFlush(packet.toPacket());
		}

	}

}

package com.rschybrid.net.packet.outgoing;

import com.rschybrid.model.item.GameItem;
import com.rschybrid.model.player.Player;
import com.rschybrid.net.packet.OutgoingPacket;
import com.rschybrid.net.packet.PacketBuilder;

public class InventoryPacket extends OutgoingPacket {

	@Override
	public void send(Player player) {
		PacketBuilder s = new PacketBuilder();
		s.setId(114);
		s.writeByte((byte)player.getInventory().getItems().size());
		for(GameItem item : player.getInventory().getItems()) {
			s.writeShort(item.getId() + (item.isWielded() ? 32768 : 0));
			if(item.getDefinitions().isStackable()) {
				s.writeInt(item.getAmount());
			}
		}
		player.getChannel().writeAndFlush(s.toPacket());
	}

}

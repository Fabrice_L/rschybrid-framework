package com.rschybrid.net.packet.outgoing;

import com.rschybrid.model.player.GameAction;
import com.rschybrid.model.player.Player;
import com.rschybrid.net.packet.OutgoingPacket;
import com.rschybrid.net.packet.PacketBuilder;

public class AppearanceScreenPacket extends OutgoingPacket {

	@Override
	public void send(Player player) {
		player.setGameAction(GameAction.CHANGING_APPEARANCE);
		PacketBuilder s = new PacketBuilder();
		s.setId(207);
		player.getChannel().writeAndFlush(s.toPacket());
	}

}

package com.rschybrid.net.packet.outgoing;

import com.rschybrid.model.player.Player;
import com.rschybrid.net.packet.OutgoingPacket;
import com.rschybrid.net.packet.PacketBuilder;
import com.rschybrid.util.Misc;

public class WorldInfoPacket extends OutgoingPacket {

	@Override
	public void send(Player player) {
		PacketBuilder s = new PacketBuilder();
		s.setId(131);
		s.writeShort(player.getId());
		s.writeShort(2304);
		s.writeShort(1776);
		s.writeShort(Misc.getHeight(player.getLocation()));
		s.writeShort(944);
		player.getChannel().writeAndFlush(s.toPacket());
	}

}

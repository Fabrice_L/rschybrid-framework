package com.rschybrid.net.packet.outgoing;

import com.rschybrid.model.player.Player;
import com.rschybrid.model.player.Stats;
import com.rschybrid.model.player.Stats.Stat;
import com.rschybrid.net.packet.OutgoingPacket;
import com.rschybrid.net.packet.PacketBuilder;

public class StatsPacket extends OutgoingPacket {

	@Override
	public void send(Player player) {
        PacketBuilder pb = new PacketBuilder();
        pb.setId(180);

        for (Stat stat : Stat.values()) {
            pb.writeByte(player.getStats().getStats().get(stat)[1]);
        }

        for (Stat stat : Stat.values()) {
            pb.writeByte(Stats.getLevelForXp(player.getStats().getStats().get(stat)[0]));
        }

        for (Stat stat : Stat.values()) {
            pb.writeInt(player.getStats().getStats().get(stat)[0]);
        }

        player.getChannel().writeAndFlush(pb.toPacket());
	}

}

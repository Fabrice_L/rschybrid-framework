package com.rschybrid.net.packet.outgoing;

import com.rschybrid.model.player.Player;
import com.rschybrid.net.packet.OutgoingPacket;
import com.rschybrid.net.packet.PacketBuilder;

public class LoginBoxPacket extends OutgoingPacket {

	@Override
	public void send(Player player) {
		PacketBuilder pb = new PacketBuilder(20);
        pb.setId(248);
        pb.writeShort(0); // Days since last login
        pb.writeShort(0); // Subscription time left
        pb.writeBytes("127.0.0.1".getBytes()); // Ip address
      	player.getChannel().writeAndFlush(pb.toPacket());
	}

}

package com.rschybrid.net.packet.outgoing;

import com.rschybrid.model.player.Player;
import com.rschybrid.net.packet.OutgoingPacket;

import io.netty.buffer.ByteBuf;
import io.netty.buffer.Unpooled;

public class RequestSessionAnswerPacket extends OutgoingPacket {
	
	private long serverKey = 0L;
	
	public RequestSessionAnswerPacket(long serverKey) {
		this.serverKey = serverKey;
	}
	
	@Override
	public void send(Player player) {
		ByteBuf buffer = Unpooled.buffer(1);
		buffer.writeLong(serverKey);
		player.getChannel().writeAndFlush(buffer);
	}

}

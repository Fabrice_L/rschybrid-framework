package com.rschybrid.net.packet.outgoing;

import com.rschybrid.model.item.GameItem;
import com.rschybrid.model.player.Player;
import com.rschybrid.net.packet.OutgoingPacket;
import com.rschybrid.net.packet.PacketBuilder;

public class UpdateItemPacket extends OutgoingPacket {
	int slot;
	
	public UpdateItemPacket(int slot) {
		this.slot = slot;
	}
	@Override
	public void send(Player player) {
		GameItem item = player.getInventory().getItems().get(slot);
		PacketBuilder s = new PacketBuilder();
		s.setId(228);
		s.writeByte((byte)slot);
		s.writeShort(item.getId() + (item.isWielded() ? 32768 : 0));
		if(item.getDefinitions().isStackable()) {
			s.writeInt(item.getAmount());
		}
		player.getChannel().writeAndFlush(s.toPacket());
	}

}

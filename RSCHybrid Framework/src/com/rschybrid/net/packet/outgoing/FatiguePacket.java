package com.rschybrid.net.packet.outgoing;

import com.rschybrid.model.player.Player;
import com.rschybrid.net.packet.OutgoingPacket;
import com.rschybrid.net.packet.PacketBuilder;

public class FatiguePacket extends OutgoingPacket {

	@Override
	public void send(Player player) {
		PacketBuilder pb = new PacketBuilder();
        pb.setId(126);
        pb.writeShort(100);
       	player.getChannel().writeAndFlush(pb.toPacket());
	}

}

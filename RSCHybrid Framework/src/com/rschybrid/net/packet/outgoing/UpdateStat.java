package com.rschybrid.net.packet.outgoing;

import com.rschybrid.model.player.Player;
import com.rschybrid.model.player.Stats;
import com.rschybrid.model.player.Stats.Stat;
import com.rschybrid.net.packet.OutgoingPacket;
import com.rschybrid.net.packet.PacketBuilder;

public class UpdateStat extends OutgoingPacket {

	private Stat stat;
	
	public UpdateStat(Stat stat) {
		this.stat = stat;
	}
	
	@Override
	public void send(Player player) {
		PacketBuilder s = new PacketBuilder();
		s.setId(208);
		s.writeByte((byte)stat.ordinal());
		s.writeByte((byte)player.getStats().getLevel(stat));
		s.writeByte((byte)Stats.getLevelForXp(player.getStats().getExperience(stat)));
		s.writeInt(player.getStats().getExperience(stat));
		player.getChannel().writeAndFlush(s.toPacket());
	}

}

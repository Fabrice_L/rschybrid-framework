package com.rschybrid.net.packet.outgoing;

import com.rschybrid.model.player.Player;
import com.rschybrid.net.packet.OutgoingPacket;
import com.rschybrid.net.packet.PacketBuilder;

public class MessagePacket extends OutgoingPacket {

	private String message;
	
	public MessagePacket(String message) {
		this.message = message;
	}
		
	@Override
	public void send(Player player) {
		PacketBuilder s = new PacketBuilder();
		s.setId(48);
		s.writeBytes(message.getBytes());
		player.getChannel().writeAndFlush(s.toPacket());
	}

}

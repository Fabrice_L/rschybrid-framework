package com.rschybrid.net.packet.outgoing;

import java.util.List;

import com.rschybrid.model.ChatMessage;
import com.rschybrid.model.item.GameItem;
import com.rschybrid.model.player.Player;
import com.rschybrid.model.player.PlayerAppearance.Appearance;
import com.rschybrid.net.packet.OutgoingPacket;
import com.rschybrid.net.packet.PacketBuilder;
import com.rschybrid.util.Misc;

public class PlayerUpdatePacket extends OutgoingPacket {

	@Override
	public void send(Player player) {
		List<Player> playersNeedingAppearanceUpdate = player.getPlayersRequiringAppearanceUpdate();
		List<ChatMessage> chatMessagesNeedingDisplayed = player.getChatMessagesNeedingDisplayed();
		int updateSize = playersNeedingAppearanceUpdate.size() + chatMessagesNeedingDisplayed.size();
		if (updateSize > 0) {
			PacketBuilder updates = new PacketBuilder();
			updates.setId(53);
			updates.writeShort(updateSize);
			for(ChatMessage cm : chatMessagesNeedingDisplayed) { // 1/6 - Player talking
				updates.writeShort(cm.getSender().getId());
				updates.writeByte((byte)(cm.getRecipient() == null ? 1 : 6));
				updates.writeByte((byte)cm.getMessage().length);
				updates.writeBytes(cm.getMessage());
			}
			for(Player p : playersNeedingAppearanceUpdate) { // 5 - Updates players appearance, clothes, skull, combat etc.
				updates.writeShort(p.getId());
				updates.writeByte((byte)5);
				updates.writeShort(p.getAppearanceID());
				updates.writeLong(Misc.usernameToHash(p.getName()));
				updates.writeByte(p.getEquipment().getCapacity());
				for (int i = 0; i < p.getEquipment().getCapacity(); i++) {
					GameItem item = p.getEquipment().getItems().get(i);
					if (item == null) {
						updates.writeByte(p.getAppearance().getSprite(i));
					} else {
						updates.writeByte(item.getWieldDefinitions().getSprite());
					}
				}
				updates.writeByte(p.getAppearance().get(Appearance.HAIR_COLOUR));//hair
				updates.writeByte(p.getAppearance().get(Appearance.TOP_COLOUR));//top
				updates.writeByte(p.getAppearance().get(Appearance.TROUSER_COLOUR));//trouser
				updates.writeByte(p.getAppearance().get(Appearance.SKIN_COLOUR));//skin
				updates.writeByte(p.getStats().getCombatLevel());
				updates.writeByte(p.isSkulled() ? 1 : 0);
				updates.writeByte(p.getRights().ordinal());
			}
			player.getChannel().writeAndFlush(updates.toPacket());
		}
	}

}

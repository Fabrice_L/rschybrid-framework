package com.rschybrid.net.packet.outgoing;

import com.rschybrid.model.item.GameItem;
import com.rschybrid.model.player.Player;
import com.rschybrid.net.packet.OutgoingPacket;
import com.rschybrid.net.packet.PacketBuilder;

public class TradeOfferPacket extends OutgoingPacket {

	@Override
	public void send(Player player) {
		Player with = player.getTrade().getTradingWith();
		if(with == null) { // This shouldn't happen
			return;
		}
	    PacketBuilder s = new PacketBuilder();
	    s.setId(250);
	    s.writeByte(with.getTradeContainer().getItems().size());
	    for(GameItem item : with.getTradeContainer().getItems()) {
	    	s.writeShort(item.getId());
	    	s.writeInt(item.getAmount());
	    }
	    player.getChannel().writeAndFlush(s.toPacket());
	}

}

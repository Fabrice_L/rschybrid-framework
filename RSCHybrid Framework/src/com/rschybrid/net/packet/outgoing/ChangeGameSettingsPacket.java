package com.rschybrid.net.packet.outgoing;

import com.rschybrid.model.player.GameSettings.GameSetting;
import com.rschybrid.model.player.Player;
import com.rschybrid.net.packet.OutgoingPacket;
import com.rschybrid.net.packet.PacketBuilder;

public class ChangeGameSettingsPacket extends OutgoingPacket {

	@Override
	public void send(Player player) {
		PacketBuilder s = new PacketBuilder();
		s.setId(152);
		s.writeByte(player.getGamesettings().get(GameSetting.CAMERA_ANGLE) ? 1 : 0);
		s.writeByte(player.getGamesettings().get(GameSetting.MOUSE_BUTTONS) ? 1 : 0);
		s.writeByte(player.getGamesettings().get(GameSetting.SOUND_EFFECTS) ? 1 : 0);
		s.writeByte(player.getGamesettings().get(GameSetting.HIDE_ROOFS)? 1 : 0);
		s.writeByte(player.getGamesettings().get(GameSetting.AUTO_SCREENSHOT) ? 1 : 0);
		s.writeByte(0);
		s.writeByte(player.getGamesettings().get(GameSetting.CERT_BANK_WITHDRAW)? 1 : 0);	
		player.getChannel().writeAndFlush(s.toPacket());
	}

}

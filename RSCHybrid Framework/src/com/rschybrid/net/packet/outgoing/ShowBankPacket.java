package com.rschybrid.net.packet.outgoing;

import com.rschybrid.model.item.GameItem;
import com.rschybrid.model.player.Player;
import com.rschybrid.model.player.container.Container.ContainerType;
import com.rschybrid.net.packet.OutgoingPacket;
import com.rschybrid.net.packet.PacketBuilder;

public class ShowBankPacket extends OutgoingPacket {

	@Override
	public void send(Player player) {
		PacketBuilder s = new PacketBuilder();
		s.setId(93);
		s.writeByte(player.getBank().getItems().size());
		s.writeByte(ContainerType.BANK.getCapacity());
		for(GameItem item : player.getBank().getItems()) {
			s.writeShort(item.getId());
			s.writeInt(item.getAmount());
		}
		player.getChannel().writeAndFlush(s.toPacket());
	}

}

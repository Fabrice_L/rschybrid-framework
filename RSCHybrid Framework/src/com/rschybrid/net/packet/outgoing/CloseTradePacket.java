package com.rschybrid.net.packet.outgoing;

import com.rschybrid.model.player.Player;
import com.rschybrid.net.packet.OutgoingPacket;
import com.rschybrid.net.packet.PacketBuilder;

public class CloseTradePacket extends OutgoingPacket {

	@Override
	public void send(Player player) {
		PacketBuilder s = new PacketBuilder();
		s.setId(187);
		player.getChannel().writeAndFlush(s.toPacket());
	}

}

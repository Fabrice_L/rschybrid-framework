package com.rschybrid.net.packet.outgoing;

import com.rschybrid.model.player.Player;
import com.rschybrid.net.packet.OutgoingPacket;

import io.netty.buffer.ByteBuf;
import io.netty.buffer.Unpooled;

public class LoginResponsePacket extends OutgoingPacket {

	private byte loginCode;
	
	public LoginResponsePacket(byte loginCode) {
		this.loginCode = loginCode;
	}
	
	@Override
	public void send(Player player) {
		ByteBuf buffer = Unpooled.buffer(1);
		buffer.writeByte(loginCode);
		player.getChannel().writeAndFlush(buffer);
		System.out.println("sent loginCode: " + loginCode);
		//player.destroy(true);
	}

}

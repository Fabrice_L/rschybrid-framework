package com.rschybrid.net.packet;

import com.rschybrid.model.player.Player;

/**
 * 
 * @author fabrice
 *
 */

public abstract class OutgoingPacket {

	public abstract void send(Player player);

}
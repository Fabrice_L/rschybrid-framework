package com.rschybrid.net.packet;

import com.rschybrid.model.player.Player;
import com.rschybrid.net.packet.incoming.BankPacket;
import com.rschybrid.net.packet.incoming.BankPacket.BankType;
import com.rschybrid.net.packet.incoming.ChangeAppearancePacket;
import com.rschybrid.net.packet.incoming.ChatPacket;
import com.rschybrid.net.packet.incoming.CommandPacket;
import com.rschybrid.net.packet.incoming.DropItemPacket;
import com.rschybrid.net.packet.incoming.FollowPacket;
import com.rschybrid.net.packet.incoming.GameSettingsPacket;
import com.rschybrid.net.packet.incoming.ItemWieldingPacket;
import com.rschybrid.net.packet.incoming.ItemWieldingPacket.WieldType;
import com.rschybrid.net.packet.incoming.LoginRequestPacket;
import com.rschybrid.net.packet.incoming.LogoutRequestPacket;
import com.rschybrid.net.packet.incoming.PickupItemPacket;
import com.rschybrid.net.packet.incoming.PingPacket;
import com.rschybrid.net.packet.incoming.PlayerAppearanceIdPacket;
import com.rschybrid.net.packet.incoming.RequestSessionPacket;
import com.rschybrid.net.packet.incoming.TradingPacket;
import com.rschybrid.net.packet.incoming.TradingPacket.TradeState;
import com.rschybrid.net.packet.incoming.WalkingPacket;

import io.netty.buffer.ByteBuf;

/**
 * 
 * @author fabrice
 *
 */

public abstract class IncomingPacket {

	private static final IncomingPacket[] packets = new IncomingPacket[255];

	public abstract void send(Player player, ByteBuf buffer);

	public static IncomingPacket[] getPackets() {
		return packets;
	}
	
	static {
		packets[32] = new RequestSessionPacket();
		packets[0] = new LoginRequestPacket();
		packets[90] = new CommandPacket();
		packets[5] = new PingPacket();
		packets[245] = new PickupItemPacket();
		packets[147] = new DropItemPacket();
		packets[181] = new ItemWieldingPacket(WieldType.WIELD);
		packets[92] = new ItemWieldingPacket(WieldType.UNWIELD);
		packets[83] = new PlayerAppearanceIdPacket();
		packets[145] = new ChatPacket();
		packets[218] = new ChangeAppearancePacket();
		packets[246] = new WalkingPacket();
		packets[132] = new WalkingPacket();
		packets[129] = new LogoutRequestPacket();
		packets[157] = new GameSettingsPacket();
		packets[198] = new BankPacket(BankType.ADD_TO_BANK);
		packets[183] = new BankPacket(BankType.REMOVE_FROM_BANK);
		packets[48] = new BankPacket(BankType.CLOSE_BANK);
		packets[166] = new TradingPacket(TradeState.REQUEST);
		packets[216] = new TradingPacket(TradeState.DECLINE);
		packets[70] = new TradingPacket(TradeState.TRADE_PROCESS);
		packets[211] = new TradingPacket(TradeState.ACCEPT_FIRST);
		packets[53] = new TradingPacket(TradeState.ACCEPT_CONFIRM);
		packets[68] = new FollowPacket();
	}
}
package com.rschybrid.net.packet;

import io.netty.buffer.ByteBuf;
import io.netty.channel.Channel;

public class Packet {
	
	private int opcode;
	private ByteBuf payload;
	private Channel channel;

	public Packet(Channel channel, ByteBuf payload) {
		this.channel = channel;
		this.payload = payload;
	}	
	
	public Packet(Channel channel, int opcode, ByteBuf payload) {
		this.channel = channel;
		this.payload = payload;
		this.opcode = opcode;
	}

	public ByteBuf getPayload() {
		return payload;
	}
	
    public static String readString(ByteBuf buffer) {
        return readString(buffer, buffer.readableBytes());
    }

    public static String readString(ByteBuf buffer, int length) {
        byte[] data = new byte[length];
        buffer.readBytes(data);
        return new String(data, 0, length);
    }
	
	public int getOpcode() {
		return opcode;
	}

	public Channel getChannel() {
		return channel;
	}

}
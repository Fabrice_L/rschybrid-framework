package com.rschybrid.net.packet.incoming;

import com.rschybrid.RSCHybrid;
import com.rschybrid.model.Point;
import com.rschybrid.model.item.GameItem;
import com.rschybrid.model.item.GroundItem;
import com.rschybrid.model.player.Player;
import com.rschybrid.model.pulse.Pulse;
import com.rschybrid.model.pulse.impl.WalkToPulse;
import com.rschybrid.net.packet.IncomingPacket;
import com.rschybrid.net.packet.outgoing.MessagePacket;
import com.rschybrid.world.ItemHandler;

import io.netty.buffer.ByteBuf;

public class PickupItemPacket extends IncomingPacket {
	
	@Override
	public void send(Player player, ByteBuf buffer) {
		int x = buffer.readShort();
		int y = buffer.readShort();
		Point location = new Point(x, y);
		int itemId = buffer.readShort();
		
  		Pulse walkTo = new WalkToPulse(location, 0) {
			@Override
			public void arrived() {
				for (int i = 0; i < ItemHandler.getGroundItems().size(); i++) {
					GroundItem item = ItemHandler.getGroundItems().get(i);
					if (item.getLocation().equals(location) && item.getId() == itemId) {
						player.getInventory().addItem(new GameItem(item.getId(), item.getAmount()));
						System.out.println(item.getId() + ": " + item.getAmount());
						ItemHandler.getGroundItems().remove(item);
						return;
					}
				}
			}
			
			public void failed() {
				player.send(new MessagePacket("You couldn't reach the item"));
			}
  		};
  		walkTo.setPulseOwner(player);
  		RSCHybrid.submit(walkTo);
	}

}

package com.rschybrid.net.packet.incoming;

import com.rschybrid.model.ChatMessage;
import com.rschybrid.model.player.Player;
import com.rschybrid.net.packet.IncomingPacket;

import io.netty.buffer.ByteBuf;

public class ChatPacket extends IncomingPacket {

	@Override
	public void send(Player player, ByteBuf buffer) {
		byte[] message = new byte[buffer.capacity()];
        buffer.readBytes(message);
        player.getChatQueue().add(new ChatMessage(player, message));
	}

}

package com.rschybrid.net.packet.incoming;

import com.rschybrid.model.item.GameItem;
import com.rschybrid.model.player.GameAction;
import com.rschybrid.model.player.Player;
import com.rschybrid.net.packet.IncomingPacket;
import com.rschybrid.net.packet.outgoing.CloseBankPacket;
import com.rschybrid.net.packet.outgoing.MessagePacket;

import io.netty.buffer.ByteBuf;

public class BankPacket extends IncomingPacket {

	private BankType bankType;
	
	public BankPacket(BankType bankType) {
		this.bankType = bankType;
	}
	
	@Override
	public void send(Player player, ByteBuf buffer) {
		if (player.getGameAction() != GameAction.BANKING) {
			player.send(new MessagePacket("@red@Trying to bank where you shouldn't, do you?"));
			return;
		}
		if (bankType == BankType.CLOSE_BANK) {
			player.send(new CloseBankPacket());
			return;
		}
		int itemId = buffer.readShort();
		int itemAmount = buffer.readInt();
		GameItem item = new GameItem(itemId, itemAmount);
		
		if (bankType == BankType.ADD_TO_BANK) {
			player.getBank().addItem(item);
		}
		
		if (bankType == BankType.REMOVE_FROM_BANK) {
			player.getBank().removeItem(item);
		}
	}
	
	public enum BankType {
		ADD_TO_BANK,
		REMOVE_FROM_BANK,
		CLOSE_BANK;
	}

}

package com.rschybrid.net.packet.incoming;

import com.rschybrid.model.player.GameAction;
import com.rschybrid.model.player.Player;
import com.rschybrid.net.packet.IncomingPacket;
import com.rschybrid.net.packet.outgoing.MessagePacket;
import com.rschybrid.world.PlayerHandler;

import io.netty.buffer.ByteBuf;

public class FollowPacket extends IncomingPacket {

	@Override
	public void send(Player player, ByteBuf buffer) {
		Player following = PlayerHandler.getPlayersOnline().get(buffer.readShort());
		
		if (following == null) {
			return;
		}
		
		if (player.getGameAction() != GameAction.NOTHING) {
			return;
		}
		
		player.setFollowing(following, 1);
		player.send(new MessagePacket("You are now following " + following.getName()));

	}

}

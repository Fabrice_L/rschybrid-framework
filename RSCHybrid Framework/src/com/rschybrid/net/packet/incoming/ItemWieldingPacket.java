package com.rschybrid.net.packet.incoming;

import com.rschybrid.model.player.Player;
import com.rschybrid.net.packet.IncomingPacket;

import io.netty.buffer.ByteBuf;

public class ItemWieldingPacket extends IncomingPacket {

	WieldType wieldType;
	
	public ItemWieldingPacket(WieldType wieldType) {
		this.wieldType = wieldType;
	}
	
	@Override
	public void send(Player player, ByteBuf buffer) {
		int itemIndex = buffer.readShort();
		if (wieldType == WieldType.WIELD) {
			player.getEquipment().addItem(player.getInventory().getItems().get(itemIndex));
		}
		if (wieldType == WieldType.UNWIELD) {
			player.getEquipment().removeItem(player.getInventory().getItems().get(itemIndex));
		}
	}
	
	public enum WieldType {
		WIELD,
		UNWIELD
	}

}

package com.rschybrid.net.packet.incoming;

import com.rschybrid.model.player.Player;
import com.rschybrid.net.packet.IncomingPacket;

import io.netty.buffer.ByteBuf;

public class PlayerAppearanceIdPacket extends IncomingPacket {

	@Override
	public void send(Player player, ByteBuf buffer) {
		int mobCount = buffer.readShort();
		int[] indicies = new int[mobCount];
		int[] appearanceIDs = new int[mobCount];
		for (int x = 0; x < mobCount; x++) {
			indicies[x] = buffer.readShort();
			appearanceIDs[x] = buffer.readShort();
		}
		player.addPlayersAppearanceIDs(indicies, appearanceIDs);
	}

}

package com.rschybrid.net.packet.incoming;

import java.math.BigInteger;

import com.rschybrid.Config;
import com.rschybrid.model.player.Player;
import com.rschybrid.net.packet.IncomingPacket;
import com.rschybrid.net.packet.Packet;
import com.rschybrid.net.packet.outgoing.LoginResponsePacket;
import com.rschybrid.util.sql.SiteIntegration;
import com.rschybrid.world.PlayerHandler;

import io.netty.buffer.ByteBuf;
import io.netty.buffer.Unpooled;

public class LoginRequestPacket extends IncomingPacket {
	
	private static final BigInteger key = new BigInteger("730546719878348732291497161314617369560443701473303681965331739205703475535302276087891130348991033265134162275669215460061940182844329219743687403068279");
	private static final BigInteger modulus = new BigInteger("1549611057746979844352781944553705273443228154042066840514290174539588436243191882510185738846985723357723362764835928526260868977814405651690121789896823");
	
	@Override
	public void send(Player player, ByteBuf buffer) {

		byte loginCode = 22;
		
		try {
			
			boolean reconnecting = buffer.readByte() == 1;
			
			int clientVersion = buffer.readShort();
			int loginPacketSize = buffer.readByte();

            byte[] encrypted = new byte[loginPacketSize];
            buffer.readBytes(encrypted);
			ByteBuf rsaData = decryptRSA(encrypted);
			int[] sessionKeys = new int[4];
			for(int key = 0;key < sessionKeys.length;key++) {
				sessionKeys[key] = rsaData.readInt();
			}
			rsaData.readInt();
			String username = Packet.readString(rsaData, 20).trim().toLowerCase().replaceAll("_", " ");
			rsaData.skipBytes(1);
			String password = Packet.readString(rsaData, 20).trim();
			
			player.setName(username);
			player.setPassword(password);

			if (username.toLowerCase().length() < 3 || password.length() < 3) {
				loginCode = 2;
				player.send(new LoginResponsePacket(loginCode));
				return;
			}
			
			int loginResult = SiteIntegration.databaseLogin(player);
			if (PlayerHandler.getPlayersOnline().size() >= Config.getMaxPlayers()) {
				loginCode = 10;
			}
			else if (clientVersion != Config.getGameVersion()) {
				loginCode = 4;
			}
			else if (!player.setSessionKeys(sessionKeys)) {
				loginCode = 5;
			}
			//wrong password, reconnecting
			else if (loginResult == 0 || reconnecting) {
				loginCode = 2;
			}
			//already logged in
			else if(PlayerHandler.isOnline(player.getName())) {
				loginCode = 3;
			} 
			//banned
			else if(loginResult == 6) {
				loginCode = 6;
			} 
			else {
				if(loginCode != 5 || loginCode != 3) {
					player.send(new LoginResponsePacket((byte) 0));
					player.initialize();
					return;
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		if(loginCode != 22) {
			player.send(new LoginResponsePacket(loginCode));
		}
	}

	public static ByteBuf decryptRSA(byte[] pData) {
        try {
            BigInteger bigInteger = new BigInteger(pData);
            byte[] decrypted = bigInteger.modPow(key, modulus).toByteArray();
            return Unpooled.wrappedBuffer(decrypted);
        } catch (Exception e) {
            return null;
        }
    }

}

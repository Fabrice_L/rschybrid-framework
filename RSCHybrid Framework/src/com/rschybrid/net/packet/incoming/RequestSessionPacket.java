package com.rschybrid.net.packet.incoming;

import com.rschybrid.model.player.Player;
import com.rschybrid.net.packet.IncomingPacket;
import com.rschybrid.net.packet.Packet;
import com.rschybrid.net.packet.outgoing.RequestSessionAnswerPacket;
import com.rschybrid.util.Misc;

import io.netty.buffer.ByteBuf;

public class RequestSessionPacket extends IncomingPacket {

	@Override
	public void send(Player player, ByteBuf buffer) {
		byte userByte = buffer.readByte();
		player.setClassName(Packet.readString(buffer));
		long serverKey = Misc.generateSessionKey(userByte);
		player.setServerKey(serverKey);	

		player.send(new RequestSessionAnswerPacket(serverKey));
	}

}

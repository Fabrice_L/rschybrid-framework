package com.rschybrid.net.packet.incoming;

import com.rschybrid.RSCHybrid;
import com.rschybrid.model.item.GameItem;
import com.rschybrid.model.player.Player;
import com.rschybrid.model.pulse.Pulse;
import com.rschybrid.model.pulse.impl.WalkToPulse;
import com.rschybrid.net.packet.IncomingPacket;
import com.rschybrid.net.packet.outgoing.MessagePacket;
import com.rschybrid.world.PlayerHandler;

import io.netty.buffer.ByteBuf;

public class TradingPacket extends IncomingPacket {
	
	private TradeState state;
	
	public TradingPacket(TradeState state) {
		this.state = state;
	}
	
	@Override
	public void send(Player player, ByteBuf buffer) {
		if (state == TradeState.REQUEST) {
			player.getTrade().setTradingWith(PlayerHandler.getPlayersOnline().get(buffer.readShort()));
			
	  		player.setFollowing(player.getTrade().getTradingWith());
	  		
	  		Pulse walkTo = new WalkToPulse(player.getTrade().getTradingWith().getLocation(), 1) {
				@Override
				public void arrived() {
					if (!player.getTrade().canTradeWith()) {
						player.getTrade().resetTrade();
						return;
					}
					player.resetFollowing();
				}
				
				public void failed() {
					player.send(new MessagePacket("You couldn't reach the player"));
					player.resetFollowing();
				}
	  		};
	  		walkTo.setPulseOwner(player);
	  		RSCHybrid.submit(walkTo);
	  		
		}
		
		if (state == TradeState.TRADE_PROCESS) {
			boolean addItem = buffer.readByte() == 1;
			GameItem item = new GameItem(buffer.readShort(), buffer.readInt());
			if (addItem) {
				player.getTrade().addItemToTrade(item);
			} else {
				player.getTrade().removeItemFromTrade(item);
			}
		}
		
		if (state == TradeState.ACCEPT_FIRST) {
			player.getTrade().acceptTrade();
		}
		
		if (state == TradeState.ACCEPT_CONFIRM) {
			player.getTrade().acceptConfirm();
		}
		
		if (state == TradeState.DECLINE) {
			player.getTrade().resetTrade();
		}
	}
	
	public enum TradeState {
		REQUEST,
		TRADE_PROCESS,
		ACCEPT_FIRST,
		ACCEPT_CONFIRM,
		DECLINE;
	}

}

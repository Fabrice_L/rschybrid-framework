package com.rschybrid.net.packet.incoming;

import java.util.HashMap;

import com.rschybrid.model.item.GameItem;
import com.rschybrid.model.player.GameAction;
import com.rschybrid.model.player.Player;
import com.rschybrid.model.player.PlayerAppearance;
import com.rschybrid.model.player.PlayerAppearance.Appearance;
import com.rschybrid.net.packet.IncomingPacket;
import com.rschybrid.net.packet.outgoing.MessagePacket;
import com.rschybrid.net.packet.outgoing.UpdateItemPacket;
import com.rschybrid.util.Misc;

import io.netty.buffer.ByteBuf;

public class ChangeAppearancePacket extends IncomingPacket {

	@Override
	public void send(Player player, ByteBuf buffer) {
		if (player.getGameAction() != GameAction.CHANGING_APPEARANCE) {
			return;
		}
		player.setGameAction(GameAction.NOTHING);
		PlayerAppearance appearance = player.getAppearance();

		HashMap<Appearance, Integer> oldAppearance = player.getAppearance().getAppearanceMap();
		
		appearance.set(Appearance.GENDER, buffer.readByte());
		appearance.set(Appearance.HEAD_SPRITE, buffer.readByte() + 1);
		appearance.set(Appearance.BODY_SPRITE, buffer.readByte() + 1);
		buffer.readByte(); 
		appearance.set(Appearance.HAIR_COLOUR, buffer.readByte());
		appearance.set(Appearance.TOP_COLOUR, buffer.readByte());
		appearance.set(Appearance.TROUSER_COLOUR, buffer.readByte());
		appearance.set(Appearance.SKIN_COLOUR, buffer.readByte());	
		
		if (!Misc.inIntArray(appearance.get(Appearance.HEAD_SPRITE), Misc.getHeadsprites())
				|| !Misc.inIntArray(appearance.get(Appearance.BODY_SPRITE), Misc.getBodysprites())) {
			appearance.setMap(oldAppearance);
			player.send(new MessagePacket("You failed to change your appearance"));
			player.setOurAppearanceChanged(true);
			return;
		}
		
		if (appearance.get(Appearance.GENDER) == 1) {
			for (GameItem item : player.getEquipment().getItems()) {
				if (item == null) continue;
				if (item.getWieldDefinitions().femaleOnly()) {
					int itemSlot = item.getWieldDefinitions().getWieldPos();
					player.getEquipment().getItems().set(itemSlot, null);
					player.send(new UpdateItemPacket(player.getInventory().getItemSlot(item)));
				}
			}
		}
		player.setOurAppearanceChanged(true);
		
	}

}

package com.rschybrid.net.packet.incoming;

import com.rschybrid.model.player.GameSettings.GameSetting;
import com.rschybrid.model.player.Player;
import com.rschybrid.net.packet.IncomingPacket;

import io.netty.buffer.ByteBuf;

public class GameSettingsPacket extends IncomingPacket {

	@Override
	public void send(Player player, ByteBuf buffer) {
		int settingId = buffer.readByte();
		boolean setting = buffer.readByte() == 1;
		if (settingId < 0 || settingId > GameSetting.values().length) {
			return;
		}
		player.getGamesettings().set(GameSetting.values()[settingId], setting);
	}

}

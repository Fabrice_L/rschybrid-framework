package com.rschybrid.net.packet.incoming;

import com.rschybrid.model.player.Player;
import com.rschybrid.net.packet.IncomingPacket;

import io.netty.buffer.ByteBuf;

public class PingPacket extends IncomingPacket {

	@Override
	public void send(Player player, ByteBuf buffer) {
		//Does nothing except for pinging
	}

}

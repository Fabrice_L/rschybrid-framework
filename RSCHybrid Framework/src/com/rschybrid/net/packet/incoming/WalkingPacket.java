package com.rschybrid.net.packet.incoming;

import com.rschybrid.RSCHybrid;
import com.rschybrid.model.Path;
import com.rschybrid.model.player.Player;
import com.rschybrid.model.pulse.Pulse;
import com.rschybrid.net.packet.IncomingPacket;

import io.netty.buffer.ByteBuf;

public class WalkingPacket extends IncomingPacket {

	@Override
	public void send(Player player, ByteBuf buffer) {
		player.resetFollowing();
		player.getTrade().resetTrade();
		for (Pulse pulse : RSCHybrid.getPulseManager().getPulses()) {
			if (pulse.getPulseName().equalsIgnoreCase("walkto") && pulse.getPulseOwner() == player) {
				pulse.stop();
			}
		}
		int startX = buffer.readShort();
		int startY = buffer.readShort();
		int numWaypoints = buffer.readableBytes() / 2;
		byte[] waypointXoffsets = new byte[numWaypoints];
		byte[] waypointYoffsets = new byte[numWaypoints];
		for (int x = 0; x < numWaypoints; x++) {
			waypointXoffsets[x] = buffer.readByte();
			waypointYoffsets[x] = buffer.readByte();
		}
		Path path = new Path(startX, startY, waypointXoffsets, waypointYoffsets);
		player.getPathHandler().setPath(path);;
	}

}

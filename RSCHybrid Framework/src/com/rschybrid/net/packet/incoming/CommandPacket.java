package com.rschybrid.net.packet.incoming;

import com.rschybrid.model.Point;
import com.rschybrid.model.item.GameItem;
import com.rschybrid.model.player.GameAction;
import com.rschybrid.model.player.Player;
import com.rschybrid.model.player.Stats.Stat;
import com.rschybrid.net.packet.IncomingPacket;
import com.rschybrid.net.packet.Packet;
import com.rschybrid.net.packet.outgoing.AppearanceScreenPacket;
import com.rschybrid.net.packet.outgoing.ShowBankPacket;
import com.rschybrid.net.packet.outgoing.UpdateStat;
import com.rschybrid.world.ItemHandler;

import io.netty.buffer.ByteBuf;

public class CommandPacket extends IncomingPacket {

	@Override
	public void send(Player player, ByteBuf buffer) {
		String command = Packet.readString(buffer);
		String[] args = command.split(" ");	
		
		if (args[0].equalsIgnoreCase("item")) {
			int id = Integer.parseInt(args[1]);
			int amount = 1;
			if (args.length == 3) {
				amount = Integer.parseInt(args[2]);
			}
			player.getInventory().addItem(new GameItem(id, amount));
		}
		
		if (args[0].equalsIgnoreCase("tele")) {
			int x = Integer.parseInt(args[1]);
			int y = Integer.parseInt(args[2]);
			player.setLocation(new Point(x, y), true);
			player.getPathHandler().resetPath();
		}
		
		if (args[0].equalsIgnoreCase("exp")) {
			player.getStats().addExperience(Stat.ATTACK, 1000);
		}
		
		if (args[0].equalsIgnoreCase("master")) {
			for (Stat stat : Stat.values()) {
				player.getStats().setStat(stat, 99);
				player.send(new UpdateStat(stat));
				player.setOurAppearanceChanged(true);
			}
		}
		
		if (args[0].equalsIgnoreCase("drop")) {
			ItemHandler.dropItem(new GameItem(10, 120), player.getLocation(), player);
			
		}
		if (args[0].equalsIgnoreCase("removedrop")) {
			ItemHandler.getGroundItems().clear();
		}
		
		if (args[0].equalsIgnoreCase("char")) {
			player.send(new AppearanceScreenPacket());
		}
		
		if (args[0].equalsIgnoreCase("bank")) {
			player.setGameAction(GameAction.BANKING);
			player.send(new ShowBankPacket());
		}
	}

}

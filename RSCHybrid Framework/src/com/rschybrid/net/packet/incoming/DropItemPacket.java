package com.rschybrid.net.packet.incoming;

import com.rschybrid.model.item.GameItem;
import com.rschybrid.model.player.Player;
import com.rschybrid.net.packet.IncomingPacket;
import com.rschybrid.world.ItemHandler;

import io.netty.buffer.ByteBuf;

public class DropItemPacket extends IncomingPacket {
	
	@Override
	public void send(Player player, ByteBuf buffer) {
		int itemIndex = buffer.readShort();
		GameItem item = player.getInventory().getItemByIndex(itemIndex);
		if (item == null) {
			return;
		}
		ItemHandler.dropItem(item, player.getLocation(), player);
		player.getInventory().removeItem(item);
	}

}

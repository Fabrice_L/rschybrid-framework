package com.rschybrid.net;

import com.rschybrid.model.player.Player;
import com.rschybrid.net.packet.Packet;

import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.SimpleChannelInboundHandler;

public class ChannelHandler extends SimpleChannelInboundHandler<Object> {

	private Player player = null;
	
	@Override
	protected void channelRead0(ChannelHandlerContext ctx, Object msg) throws Exception {
		if (!ctx.channel().isActive()) {
			return;
		}
		if (msg instanceof Packet) {
			if (((Packet) msg).getOpcode() == 32) {
				player = new Player(ctx.channel());
			}
			player.getPacketManager().queuePacket((Packet) msg);
		}

	}
	
	@Override
	public void channelInactive(ChannelHandlerContext ctx) throws Exception {
		player.destroy();
	}
	
	@Override
	public void exceptionCaught(ChannelHandlerContext ctx, Throwable cause) throws Exception {
		ctx.channel().close();
	}

}
package com.rschybrid.model.pulse;

import com.rschybrid.RSCHybrid;
import com.rschybrid.model.Entity;

public abstract class Pulse {

	private PulseOwner owner;
	private String pulseName;
	private int remainingPulses;
	private int pulseDelay;
	private boolean immediate;
	private boolean running = true;
	
	public Pulse(int pulses, String pulseName) {
		this.setRemainingPulses(pulses);
		this.setPulseDelay(pulses);
		this.setPulseName(pulseName);
	}
	
	public Pulse(int pulses, String pulseName, boolean immediate) {
		this(pulses, pulseName);
		this.immediate = immediate;
	}
	
	public abstract void execute();
	
	public void cycle() {
		if(remainingPulses-- <= 1 || immediate) {
			immediate = false;
			remainingPulses = pulseDelay;
			if(isRunning()) {
				execute();
			}
		}
	}
	
	public void pulseExists() {
		for (Pulse pulse : RSCHybrid.getPulseManager().getPulses()) {
			if (pulse.getPulseOwner() == this.getPulseOwner()
					&& pulse.getPulseName().equals(this.getPulseName())) {
				pulse.stop();
			}
		}
	}
	
	public int getRemainingPulses() {
		return remainingPulses;
	}
	
	public void setRemainingPulses(int remainingPulses) {
		this.remainingPulses = remainingPulses;
	}

	public int getPulseDelay() {
		return pulseDelay;
	}

	public void setPulseDelay(int pulseDelay) {
		this.pulseDelay = pulseDelay;
	}

	public boolean isRunning() {
		return running;
	}

	public void setRunning(boolean running) {
		this.running = running;
	}
	
	public void stop() {
		running = false;
	}

	public Entity getPulseOwner() {
		return owner.getOwner();
	}

	public void setPulseOwner(Entity owner) {
		this.owner = new PulseOwner(owner);
	}

	public String getPulseName() {
		return pulseName;
	}

	public void setPulseName(String pulseName) {
		this.pulseName = pulseName;
	}
	
}

package com.rschybrid.model.pulse;

import java.util.LinkedList;
import java.util.List;

public class PulseManager {

	private List<Pulse> pulses = new LinkedList<Pulse>();

	public List<Pulse> getPulses() {
		return pulses;
	}

	public void submit(final Pulse pulse) {
		pulse.pulseExists();
		pulses.add(pulse);
	}
}

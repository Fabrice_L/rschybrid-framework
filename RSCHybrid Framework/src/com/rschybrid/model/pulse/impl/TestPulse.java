package com.rschybrid.model.pulse.impl;

import com.rschybrid.model.pulse.Pulse;

public class TestPulse extends Pulse {

	int pulses = 10;
	public TestPulse() {
		super(3, "test", true);
	}

	@Override
	public void execute() {
		//Npc npc = (Npc)getPulseOwner().getOwner();
		pulses --;
		if (pulses <= 0) {
			System.out.println("Last pulse sent");
			this.stop();
			return;
		}
	}

}

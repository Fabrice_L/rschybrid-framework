package com.rschybrid.model.pulse.impl;

import com.rschybrid.model.Point;
import com.rschybrid.model.pulse.Pulse;

public abstract class WalkToPulse extends Pulse {

	private Point location;
	private int radius;
	private Point lastLocation;
	private int failRuns = 5;
	
	public WalkToPulse(Point location, int radius) {
		super(1, "walkto", true);
		this.location = location;
		this.radius = radius;
	}

	@Override
	public void execute() {
		if(getPulseOwner().getLocation().withinDistance(location, radius)) {
			arrived();
		}
		else if (failRuns == 0) {
			failed();
		}
		else {
			if (lastLocation == getPulseOwner().getLocation()) {
				failRuns--;
				return;
			}
			lastLocation = getPulseOwner().getLocation();
			return;
		}
		this.stop();
	}
	
	public abstract void arrived();
	
	public void failed() { }

}

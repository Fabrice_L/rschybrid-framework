package com.rschybrid.model.pulse;

import com.rschybrid.model.Entity;

public class PulseOwner {

	private Entity owner;
	
	public PulseOwner(Entity owner) {
		this.owner = owner;
	}
	
	public Entity getOwner() {
		return owner;
	}

}
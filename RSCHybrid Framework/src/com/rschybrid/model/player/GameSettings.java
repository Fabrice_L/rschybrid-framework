package com.rschybrid.model.player;

import java.util.HashMap;

public class GameSettings {

	private HashMap<GameSetting, Boolean> gameSettings = new HashMap<GameSetting, Boolean>(GameSetting.values().length);
	
	public GameSettings() {
		set(GameSetting.CAMERA_ANGLE, false);
		set(GameSetting.MOUSE_BUTTONS, false);
		set(GameSetting.SOUND_EFFECTS, false);
		set(GameSetting.HIDE_ROOFS, false);
		set(GameSetting.AUTO_SCREENSHOT, false);
		set(GameSetting.CERT_BANK_WITHDRAW, false);
	}
	
	public boolean get(GameSetting setting) {
		return gameSettings.get(setting);
	}
	
	public void set(GameSetting setting, Boolean bool) {
		gameSettings.put(setting, bool);
	}
	
	public enum GameSetting {
		CAMERA_ANGLE,
		EMPTY,
		MOUSE_BUTTONS,
		SOUND_EFFECTS,
		HIDE_ROOFS,
		AUTO_SCREENSHOT,
		EMPTY2,
		CERT_BANK_WITHDRAW;
	}
}

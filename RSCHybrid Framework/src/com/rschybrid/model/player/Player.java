package com.rschybrid.model.player;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;

import com.rschybrid.Config;
import com.rschybrid.model.ChatMessage;
import com.rschybrid.model.Entity;
import com.rschybrid.model.Point;
import com.rschybrid.model.item.GroundItem;
import com.rschybrid.model.player.container.impl.BankContainer;
import com.rschybrid.model.player.container.impl.EquipmentContainer;
import com.rschybrid.model.player.container.impl.InventoryContainer;
import com.rschybrid.model.player.container.impl.TradeContainer;
import com.rschybrid.net.PacketManager;
import com.rschybrid.net.packet.IncomingPacket;
import com.rschybrid.net.packet.OutgoingPacket;
import com.rschybrid.net.packet.outgoing.AppearanceScreenPacket;
import com.rschybrid.net.packet.outgoing.ChangeGameSettingsPacket;
import com.rschybrid.net.packet.outgoing.FatiguePacket;
import com.rschybrid.net.packet.outgoing.InventoryPacket;
import com.rschybrid.net.packet.outgoing.LoginBoxPacket;
import com.rschybrid.net.packet.outgoing.LogoutPacket;
import com.rschybrid.net.packet.outgoing.MessagePacket;
import com.rschybrid.net.packet.outgoing.ServerInfoPacket;
import com.rschybrid.net.packet.outgoing.StatsPacket;
import com.rschybrid.net.packet.outgoing.WorldInfoPacket;
import com.rschybrid.util.StatefulEntityCollection;
import com.rschybrid.world.ItemHandler;
import com.rschybrid.world.PlayerHandler;

import io.netty.buffer.ByteBuf;
import io.netty.channel.Channel;

public class Player extends Entity {
	
	private Channel channel;
	private PacketManager packetManager = new PacketManager(this);
	
	private String password;
	private boolean loggedIn;
	private long lastLogin = 0L;
	private boolean skulled;
	private Rights rights = Rights.REGULAR_MEMBER;
	private GameAction gameAction = GameAction.NOTHING;
	private InventoryContainer inventory = new InventoryContainer(this);
	private EquipmentContainer equipment = new EquipmentContainer(this);
	private BankContainer bank = new BankContainer(this);
	private TradeContainer tradeContainer = new TradeContainer(this);
	private PlayerAppearance appearance = new PlayerAppearance();
	private Stats stats = new Stats(this);
	private GameSettings gamesettings = new GameSettings();
	private Trade trade = new Trade(this);
	
	private String className = "";
	public static int[] sessionKeys = new int[4];
	
	private StatefulEntityCollection<GroundItem> watchedItems = new StatefulEntityCollection<GroundItem>();
	private StatefulEntityCollection<Player> watchedPlayers = new StatefulEntityCollection<Player>();
	private HashMap<Integer, Integer> knownPlayersAppearanceIDs = new HashMap<Integer, Integer>();
	
	private LinkedList<ChatMessage> chatQueue = new LinkedList<ChatMessage>();
	private ArrayList<ChatMessage> chatMessagesNeedingDisplayed = new ArrayList<ChatMessage>();
	
	public Player(Channel channel) {
		this.channel = channel;
		PlayerHandler.getPlayerSessions().add(this);
	}
	
	public void initialize() {
		//player.load(username, password, uid, reconnecting);
		System.out.println("Succesful Login!");
		setId(PlayerHandler.getFreeIndex());
		setLocation(Point.location(580 + (int)(Math.random() * 5), 749));

		if (getLastLogin() == 0L) {
			send(new AppearanceScreenPacket());
		}
		send(new MessagePacket("@ran@Welcome to " + Config.getServerName()));

		PlayerHandler.getPlayersOnline().add(this);
		send(new ChangeGameSettingsPacket());
		send(new ServerInfoPacket());
	    send(new FatiguePacket());
	    send(new WorldInfoPacket());
	    send(new StatsPacket());
	    send(new LoginBoxPacket());
	    send(new InventoryPacket());
	    setLastLogin(System.currentTimeMillis());
		setLoggedIn(true);		
	}
	
	public void send(IncomingPacket incomingPacket, ByteBuf buffer) {
		incomingPacket.send(this, buffer);		
	}
	
	public void send(OutgoingPacket outgoingPacket) {
		outgoingPacket.send(this);
	}
	
	public void destroy() {
		this.setLoggedIn(false);
		PlayerHandler.getPlayerSessions().remove(this);
		PlayerHandler.getPlayersOnline().remove(this);
		if (gameAction == GameAction.TRADING) {
			trade.resetTrade();
		}
		this.send(new LogoutPacket());
	}

	public PacketManager getPacketManager() {
		return packetManager;
	}

	public String getClassName() {
		return className;
	}

	public void setClassName(String className) {
		this.className = className;
	}
	
	public void setServerKey(long key) {
		sessionKeys[2] = (int)(key >> 32);
		sessionKeys[3] = (int)key;
	}
	
	public boolean setSessionKeys(int[] keys) {
		boolean valid = (sessionKeys[2] == keys[2] && sessionKeys[3] == keys[3]);
		sessionKeys = keys;
		return valid;
	}

	public Channel getChannel() {
		return channel;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public boolean isLoggedIn() {
		return loggedIn;
	}

	public void setLoggedIn(boolean loggedIn) {
		this.loggedIn = loggedIn;
	}
	
	public StatefulEntityCollection<Player> getWatchedPlayers() {
		return watchedPlayers;
	}
	
	public void addPlayersAppearanceIDs(int[] indicies, int[] appearanceIDs) {
		for (int x = 0; x < indicies.length; x++) {
			knownPlayersAppearanceIDs.put(indicies[x], appearanceIDs[x]);
		}
	}
	
	public void updateViewedPlayers() {
		for (Player player : PlayerHandler.getPlayersOnline()) {
			if (player.getLocation().withinDistance(this.getLocation())) {
				if (player.getId() != this.getId() && player.isLoggedIn()) {
					player.informOfPlayer(this);
					this.informOfPlayer(player);
				}
			}
		}
		for (Player player : watchedPlayers.getKnownEntities()) {
			if (!this.getLocation().withinDistance(player.getLocation()) || !player.isLoggedIn()) {
				watchedPlayers.remove(player);
				knownPlayersAppearanceIDs.remove(player.getId());
			}
		}
	}
	
	public List<Player> getPlayersRequiringAppearanceUpdate() {
		List<Player> needingUpdates = new ArrayList<Player>();
		needingUpdates.addAll(watchedPlayers.getNewEntities());
		if (isOurAppearanceChanged()) {
			needingUpdates.add(this);
		}
		for (Player p : watchedPlayers.getKnownEntities()) {
			if (needsAppearanceUpdateFor(p)) {
				needingUpdates.add(p);
			}
		}
		return needingUpdates;
	}
	
	private boolean needsAppearanceUpdateFor(Player p) {
		int playerServerIndex = p.getId();
		if (knownPlayersAppearanceIDs.containsKey(playerServerIndex)) {
			int knownPlayerAppearanceID = knownPlayersAppearanceIDs.get(playerServerIndex);
			if(knownPlayerAppearanceID != p.getAppearanceID()) {
				return true;
			}
		}
		else {
			return true;
		}
		return false;
	}
	
	public void informOfPlayer(Player p) {
		if ((!watchedPlayers.contains(p) || watchedPlayers.isRemoving(p))) {
			watchedPlayers.add(p);
		}
	}
	
	public void updateMessageQueues() {
		for(Player sender : PlayerHandler.getPlayersOnline()) {
			ChatMessage message = sender.getChatQueue().poll();
			if(message == null || !sender.isLoggedIn()) {
				continue;
			}
			for(Player recipient : PlayerHandler.getPlayersOnline()) {
				if (recipient.getLocation().withinDistance(sender.getLocation()))
				if(sender.getId() == recipient.getId() || !recipient.isLoggedIn()) {
					continue;
				}
				recipient.chatMessagesNeedingDisplayed.add(message);
			}
		}
	}
	
	public void updateViewedItems() {
		for (GroundItem item : ItemHandler.getGroundItems()) {
			if (item.visibleTo(this) && item.getLocation().withinDistance(this.getLocation()) && !watchedItems.contains(item)) {
				watchedItems.add(item);
			}
		}
		
		for (GroundItem item : watchedItems.getKnownEntities()) {
			if (!ItemHandler.getGroundItems().contains(item) ||!item.getLocation().withinDistance(this.getLocation())) {
				watchedItems.remove(item);
			}
		}
	}

	public InventoryContainer getInventory() {
		return inventory;
	}
	
	public void setInventory(InventoryContainer inventory) {
		this.inventory = inventory;
	}

	public EquipmentContainer getEquipment() {
		return equipment;
	}

	public PlayerAppearance getAppearance() {
		return appearance;
	}

	public long getLastLogin() {
		return lastLogin;
	}

	public void setLastLogin(long lastLogin) {
		this.lastLogin = lastLogin;
	}

	public LinkedList<ChatMessage> getChatQueue() {
		return chatQueue;
	}

	public ArrayList<ChatMessage> getChatMessagesNeedingDisplayed() {
		return chatMessagesNeedingDisplayed;
	}

	public Stats getStats() {
		return stats;
	}

	public Rights getRights() {
		return rights;
	}

	public void setRights(Rights rights) {
		this.rights = rights;
	}

	public boolean isSkulled() {
		return skulled;
	}

	public void setSkulled(boolean skulled) {
		this.skulled = skulled;
	}

	public GameSettings getGamesettings() {
		return gamesettings;
	}

	public void setGamesettings(GameSettings gamesettings) {
		this.gamesettings = gamesettings;
	}
	
	public BankContainer getBank() {
		return bank;
	}

	public GameAction getGameAction() {
		return gameAction;
	}

	public void setGameAction(GameAction gameAction) {
		this.gameAction = gameAction;
	}

	public Trade getTrade() {
		return trade;
	}

	public TradeContainer getTradeContainer() {
		return tradeContainer;
	}

	public StatefulEntityCollection<GroundItem> getWatchedItems() {
		return watchedItems;
	}

	public void setWatchedItems(StatefulEntityCollection<GroundItem> watchedItems) {
		this.watchedItems = watchedItems;
	}

}

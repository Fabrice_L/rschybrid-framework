package com.rschybrid.model.player;

public enum GameAction {
	NOTHING,
	BANKING,
	CHANGING_APPEARANCE,
	TRADING;
}

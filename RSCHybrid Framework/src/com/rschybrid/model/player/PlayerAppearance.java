package com.rschybrid.model.player;

import java.util.HashMap;

public class PlayerAppearance {
	
	private HashMap<Appearance, Integer> appearanceMap = new HashMap<Appearance, Integer>(Appearance.values().length);
	
	public PlayerAppearance() {
		appearanceMap.put(Appearance.GENDER, 1);
		appearanceMap.put(Appearance.HEAD_SPRITE, 1);
		appearanceMap.put(Appearance.BODY_SPRITE, 2);
		appearanceMap.put(Appearance.HAIR_COLOUR, 2);
		appearanceMap.put(Appearance.TOP_COLOUR, 8);
		appearanceMap.put(Appearance.TROUSER_COLOUR, 14);
		appearanceMap.put(Appearance.SKIN_COLOUR, 0);
	}
	
	public int get(Appearance appearance) {
		return appearanceMap.get(appearance);
	}
	
	public void set(Appearance appearance, int id) {
		appearanceMap.put(appearance, id);
	}
	
	public HashMap<Appearance, Integer> getAppearanceMap() {
		return appearanceMap;
	}
	
	public void setMap(HashMap<Appearance, Integer> map) {
		appearanceMap = map;
	}
	
	public int getSprite(int pos) {
		switch(pos) {
			case 0:
				return appearanceMap.get(Appearance.HEAD_SPRITE); //head
			case 1:
				return appearanceMap.get(Appearance.BODY_SPRITE); //body
			case 2:
				return 3;
			default:
				return 0;
		}
	}
	
	public enum Appearance {
		GENDER,
		HEAD_SPRITE,
		BODY_SPRITE,
		HAIR_COLOUR,
		TOP_COLOUR,
		TROUSER_COLOUR,
		SKIN_COLOUR;
	}

}
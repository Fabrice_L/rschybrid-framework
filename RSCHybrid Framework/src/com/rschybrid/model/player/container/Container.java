package com.rschybrid.model.player.container;

import java.util.ArrayList;

import com.rschybrid.model.item.GameItem;
import com.rschybrid.model.player.Player;
import com.rschybrid.net.packet.outgoing.MessagePacket;

public abstract class Container {

	private Player player;
	
	private int capacity;
	
	//private GameItem[] items;
	private ArrayList<GameItem> items = new ArrayList<GameItem>();
	
	public Container(Player player, int capacity) {
		this.player = player;
		this.capacity = capacity;	
	}
	
	public ArrayList<GameItem> getItems() {
		return items;
	}
	
	public void setItems(ArrayList<GameItem> items) {
		this.items = items;
	}

	public abstract boolean addItem(GameItem item);
	
	public abstract boolean removeItem(GameItem item);
			
	public int getItemSlot(GameItem item) {
		for (int slot = 0; slot < getItems().size(); slot++) {
			if (getItems().get(slot).getId() == item.getId()) {
				return slot;
			}
		}
		return 0;	
	}
	
	public GameItem getItemByIndex(int index) {
		GameItem item = getItems().get(index);
		if(item == null) {
			return null;
		}
		return item;
	}
		
	public boolean containsItem(GameItem item) {
		for (GameItem items : getItems()) {
			if (items == null) continue;
			if (items.getId() == item.getId()) {
				return true;
			}
		}
		return false;
	}
	
	public int getFirstIndexById(int id) {
		for(int index = 0;index < getItems().size();index++) {
			if(getItems().get(index).getId() == id) {
				return index;
			}
		}
		return -1;
	}
	
	public boolean containsItemAmount(GameItem item) {
		for (GameItem items : getItems()) {
			if (items.getId() == item.getId() && items.getAmount() == item.getAmount()) {
				return true;
			}
		}
		getPlayer().send(new MessagePacket("You don't have enough " + item.getDefinitions().getName() + " stored to do this"));
		return false;
	}
	
	public boolean hasFreeSlots() {
		if (getItems().size() < capacity) {
			return true;
		}
		return false;
	}
	
	public int freeSlotAmount() {
		return capacity - getItems().size();
	}
	
	public int getItemAmount(GameItem item, boolean stacked) {
		int amount = 0;
		for(GameItem items : getItems()) {
			if(items.getId() == item.getId()) {
				if (stacked) {
					return items.getAmount();
				}
				amount++;
			}
		}
		return amount;
	}

	public Player getPlayer() {
		return player;
	}

	public void setPlayer(Player player) {
		this.player = player;
	}
	
	public int getCapacity() {
		return capacity;
	}
	
	public enum ContainerType {
		BANK(192),
		INVENTORY(32),
		TRADE(32),
		EQUIPMENT(12);
		
		private int capacity;
		
		ContainerType(int capacity) {
			this.capacity = capacity;
		}
		
		public int getCapacity() {
			return capacity;
		}

	}
}
package com.rschybrid.model.player.container.impl;

import com.rschybrid.model.item.Certificates;
import com.rschybrid.model.item.GameItem;
import com.rschybrid.model.player.GameSettings.GameSetting;
import com.rschybrid.model.player.Player;
import com.rschybrid.model.player.container.Container;
import com.rschybrid.net.packet.outgoing.InventoryPacket;
import com.rschybrid.net.packet.outgoing.MessagePacket;
import com.rschybrid.net.packet.outgoing.ShowBankPacket;
import com.rschybrid.net.packet.outgoing.UpdateBankItemPacket;

public class BankContainer extends Container {

	public BankContainer(Player player) {
		super(player, ContainerType.BANK.getCapacity());
	}

	@Override
	public boolean addItem(GameItem item) {
		if (item.getId() < 0 || item.getAmount() < 0) {
			return false;
		}
		
		if (!getPlayer().getInventory().containsItem(item)) {
			return false;
		}
		
		boolean inventoryStacked = item.getDefinitions().isStackable();
		if (getPlayer().getInventory().getItemAmount(item, inventoryStacked) < item.getAmount()) {
			item.setAmount(getPlayer().getInventory().getItemAmount(item, inventoryStacked));
		}
		if (!hasFreeSlots() && !containsItem(item)) {
			getPlayer().send(new MessagePacket("There isn't enough storage space to do this!"));
			return false;
		}
		
		GameItem newItem = new GameItem(Certificates.getUncertId(item.getId()), item.getAmount());
		
		
		if (getPlayer().getEquipment().containsItem(item)) {
			getPlayer().getEquipment().removeItem(item);
		}
		if(containsItem(newItem) && getItemSlot(newItem) >= 0) {
			if (getItems().get(getItemSlot(newItem)).addAmount(newItem.getAmount()) < 1) {
				getItems().get(getItemSlot(newItem)).setAmount(Integer.MAX_VALUE);
			}
		} else {
			getItems().add(newItem);
		}

		getPlayer().getInventory().removeItem(item);

		getPlayer().send(new InventoryPacket());
		getPlayer().send(new UpdateBankItemPacket(getItems().get(getItemSlot(newItem))));
		
		return true;
	}

	@Override
	public boolean removeItem(GameItem item) {
		if (item.getId() < 0 || item.getAmount() < 0) {
			return false;
		}
		
		if (!containsItem(item)) {
			return false;
		}
		
		if (getItemAmount(item, true) < item.getAmount()) {
			item.setAmount(getItemAmount(item, true));
		}
		
		boolean toCert = getPlayer().getGamesettings().get(GameSetting.CERT_BANK_WITHDRAW);
		int certedId = Certificates.getCertId(item.getId());
		
		GameItem newItem = new GameItem(toCert ? (certedId > 0 ? certedId : item.getId())  : item.getId(), item.getAmount());
		
		if (certedId < 0) {
			getPlayer().send(new MessagePacket("This item does not have a certificate!"));
		}
		
		if (newItem.getAmount() > getPlayer().getInventory().freeSlotAmount() && !newItem.getDefinitions().isStackable()) {
			newItem.setAmount(getPlayer().getInventory().freeSlotAmount());
			getPlayer().send(new MessagePacket("Not enough room to add that amount of items!"));
		}
		
		int itemSlot = getItemSlot(item);
		if (getItems().get(itemSlot).removeAmount(newItem.getAmount()) < 1) {
			getItems().remove(itemSlot);
		}
		
		
		System.out.println("trying to add: " + newItem.getId());
		getPlayer().getInventory().addItem(newItem);
		
		getPlayer().send(new InventoryPacket());
		getPlayer().send(new ShowBankPacket());	
		return true;
	}

}

package com.rschybrid.model.player.container.impl;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Map.Entry;

import com.rschybrid.model.item.GameItem;
import com.rschybrid.model.item.def.ItemWieldableDef;
import com.rschybrid.model.player.Player;
import com.rschybrid.model.player.PlayerAppearance.Appearance;
import com.rschybrid.model.player.Stats.Stat;
import com.rschybrid.model.player.container.Container;
import com.rschybrid.net.packet.outgoing.MessagePacket;
import com.rschybrid.net.packet.outgoing.UpdateItemPacket;

public class EquipmentContainer extends Container {

	public EquipmentContainer(Player player) {
		super(player, ContainerType.EQUIPMENT.getCapacity());
		this.setItems(new ArrayList<GameItem>(Collections.nCopies(getCapacity(), null)));
	}

	@Override
	public boolean addItem(GameItem item) {
		int itemSlot = item.getWieldDefinitions().getWieldPos();
		
		if (getPlayer().getAppearance().get(Appearance.GENDER) == 1 && item.getWieldDefinitions().femaleOnly()) {
			getPlayer().send(new MessagePacket("You can't wear this item, it's female only!"));
			return false;
		}
		
		for (Entry<Integer, Integer> levelNeeded : item.getWieldDefinitions().getStatsRequired()) {
			Stat stat = Stat.values()[levelNeeded.getKey()];
			int levelRequired = levelNeeded.getValue();
			if (getPlayer().getStats().getLevel(stat) < levelRequired) {
				for (Entry<Integer, Integer> levelNeededList : item.getWieldDefinitions().getStatsRequired()) {
					Stat statList = Stat.values()[levelNeededList.getKey()];
					int levelRequiredList = levelNeededList.getValue();
					getPlayer().send(new MessagePacket("You need a " + statList.toString().toLowerCase() + " level of " + levelRequiredList + " to use this item!"));
				}
				return false;
			}
		}
		
		for (GameItem items : getItems()) {
			if (items == null) continue;
			for (int affected : item.getWieldDefinitions().getAffectedTypes()) {
				if (items.getWieldDefinitions().getType() == affected) {
					removeItem(items);
				}
			}
		}
		GameItem updateItem = getPlayer().getInventory().getItemByIndex(getPlayer().getInventory().getItemSlot(item));
		updateItem.setWielded(true);
		getItems().set(itemSlot, updateItem);
		getPlayer().setOurAppearanceChanged(true);
		getPlayer().send(new UpdateItemPacket(getPlayer().getInventory().getItemSlot(updateItem)));
		return true;
	}

	@Override
	public boolean removeItem(GameItem item) {
		if (!containsItem(item)) {
			return false;
		}
		item.setWielded(false);
		int itemSlot = ItemWieldableDef.getDef(item.getId()).getWieldPos();
		getItems().set(itemSlot, null);
		getPlayer().setOurAppearanceChanged(true);
		getPlayer().send(new UpdateItemPacket(getPlayer().getInventory().getItemSlot(item)));
		return true;
	}

}

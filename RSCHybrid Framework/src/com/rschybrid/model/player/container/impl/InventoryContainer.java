package com.rschybrid.model.player.container.impl;

import com.rschybrid.model.item.GameItem;
import com.rschybrid.model.player.Player;
import com.rschybrid.model.player.container.Container;
import com.rschybrid.net.packet.outgoing.InventoryPacket;
import com.rschybrid.net.packet.outgoing.MessagePacket;

public class InventoryContainer extends Container{

	public InventoryContainer(Player player) {
		super(player, ContainerType.INVENTORY.getCapacity());
	}

	@Override
	public boolean addItem(GameItem item) {
		if(item.getAmount() < 1) {
			return false;
		}
		
		if(!hasFreeSlots() && !(containsItem(item) && item.getDefinitions().isStackable())) {
			getPlayer().send(new MessagePacket("There isn't enough storage space to do this!"));
			return false;
		}
		if(containsItem(item) && item.getDefinitions().isStackable()) {
			int itemSlot = getItemSlot(item);
			if (getItems().get(itemSlot).addAmount(item.getAmount()) < 1) {
				getItems().get(itemSlot).setAmount(Integer.MAX_VALUE);
			}
		} else {
			if (item.getAmount() == 1) {
				getItems().add(item);
			} else {
				int amount = item.getAmount();
				if(!item.getDefinitions().isStackable()) {
					while (amount > 0) {
						if (hasFreeSlots()) {
							getItems().add(new GameItem(item.getId(), 1));
							amount--;
						} else {
							amount = 0;
							getPlayer().send(new MessagePacket("There isn't enough storage space to do this!"));
						}
					}
				} else {
					if(hasFreeSlots()) {
						getItems().add(item);
					} else {
						getPlayer().send(new MessagePacket("There isn't enough storage space to do this!"));
					}
				}
			}
		}
		getPlayer().send(new InventoryPacket());
		return true;
	}

	@Override
	public boolean removeItem(GameItem item) {
		if (!containsItem(item)) {
			return false;
		}
		
		if (item.getAmount() < 1) {
			return false;
		}
		
		if (getPlayer().getEquipment().containsItem(item)) {
			getPlayer().getEquipment().removeItem(item);
		}
		
		int itemSlot = getItemSlot(item);
		if (item.getDefinitions().isStackable()) {
			if (getItems().get(itemSlot).removeAmount(item.getAmount()) < 1) {
				getItems().remove(itemSlot);
			}
		} else {
			if (item.getAmount() == 1) {
				getItems().remove(itemSlot);
			} else {
				int amount = item.getAmount();
				while (containsItem(item) && amount > 0) {
					getItems().remove(getItemSlot(item));
					amount--;
				}
			}
		}
		getPlayer().send(new InventoryPacket());
		return true;
	}

}
package com.rschybrid.model.player;

public enum Rights {
	REGULAR_MEMBER,
	PLAYER_MOD,
	MODERATOR,
	ADMIN;
}

package com.rschybrid.model.player;

import java.util.ArrayList;

import com.rschybrid.model.item.GameItem;
import com.rschybrid.net.packet.outgoing.CloseTradePacket;
import com.rschybrid.net.packet.outgoing.InventoryPacket;
import com.rschybrid.net.packet.outgoing.MessagePacket;
import com.rschybrid.net.packet.outgoing.ShowTradeAcceptPacket;
import com.rschybrid.net.packet.outgoing.ShowTradePacket;
import com.rschybrid.net.packet.outgoing.TradeOfferAcceptPacket;
import com.rschybrid.net.packet.outgoing.TradeOfferPacket;

public class Trade {

	private Player player;
	private Player tradingWith;
	private boolean offerAccepted;
	private boolean confirmAccepted;
	private ArrayList<GameItem> inventoryCopy = new ArrayList<GameItem>(32);

	
	public Trade(Player player) {
		this.player = player;
	}
	
	public boolean canTradeWith() {
		if (tradingWith == player) {
			player.send(new MessagePacket("You can't trade yourself!"));
			return false;
		}
		if (!player.getLocation().withinDistance(tradingWith.getLocation(), 3)) {
			player.send(new MessagePacket("Could not reach this player!"));
			return false;
		}
		if (player.getGameAction() == GameAction.TRADING) {
			player.send(new MessagePacket("You are in a trade session already!"));
			return false;
		}
		if (tradingWith.getGameAction() == GameAction.TRADING) {
			player.send(new MessagePacket("This player is in a trade session already!"));
			return false;
		}
		if (tradingWith.getTrade().getTradingWith() == player) {
			startTrade();
			return true;
		}
		player.send(new MessagePacket("Sending trade request..."));
		tradingWith.send(new MessagePacket(player.getName() +" would like to trade with you."));
		return true;
	}
	
	public void startTrade() {
		
		player.setGameAction(GameAction.TRADING);
		tradingWith.setGameAction(GameAction.TRADING);
		
		player.getPathHandler().resetPath();
		tradingWith.getPathHandler().resetPath();
		
		player.send(new ShowTradePacket());
		tradingWith.send(new ShowTradePacket());
		player.getTrade().inventoryCopy = createOldInventory(player);
		tradingWith.getTrade().inventoryCopy = createOldInventory(tradingWith);
		player.send(new MessagePacket("You accepted " + tradingWith.getName() + "'s trade request"));
		tradingWith.send(new MessagePacket(player.getName() + " accepted your trade request"));
	}
	
	public void addItemToTrade(GameItem item) {
		if (tradingWith == player || tradingWith == null || player.getGameAction() != GameAction.TRADING) {
			resetTrade();
			return;
		}
		player.getTradeContainer().addItem(item);
		tradingWith.send(new TradeOfferPacket());
		
		player.getTrade().setOfferAccepted(false);
		player.getTrade().setConfirmAccepted(false);
		tradingWith.getTrade().setOfferAccepted(false);
		tradingWith.getTrade().setConfirmAccepted(false);
	}
	
	public void removeItemFromTrade(GameItem item) {
		if (tradingWith == player || tradingWith == null || player.getGameAction() != GameAction.TRADING) {
			resetTrade();
			return;
		}
		player.getTradeContainer().removeItem(item);
		tradingWith.send(new TradeOfferPacket());
		
		player.getTrade().setOfferAccepted(false);
		player.getTrade().setConfirmAccepted(false);
		tradingWith.getTrade().setOfferAccepted(false);
		tradingWith.getTrade().setConfirmAccepted(false);
	}
	
	public void acceptTrade() {
		if (tradingWith == player || tradingWith == null || player.getGameAction() != GameAction.TRADING) {
			resetTrade();
			return;
		}
		offerAccepted = true;
		player.send(new TradeOfferAcceptPacket());
		tradingWith.send(new TradeOfferAcceptPacket());
		
		if (tradingWith.getTrade().isOfferAccepted()) {
			player.send(new ShowTradeAcceptPacket());
			tradingWith.send(new ShowTradeAcceptPacket());
		}
		
	}
	
	public void acceptConfirm() {
		if (tradingWith == player || tradingWith == null || player.getGameAction() != GameAction.TRADING) {
			resetTrade();
			return;
		}
		confirmAccepted = true;
		
		if (tradingWith.getTrade().isConfirmAccepted()) {
			for (GameItem item : player.getTradeContainer().getItems()) {
				if (!tradingWith.getInventory().addItem(item)) {
					player.send(new MessagePacket("The other player did not have enough open inventory slots"));
					resetTrade();
					return;
				}
				//player.getInventory().removeItem(item);
			}
			for (GameItem item : tradingWith.getTradeContainer().getItems()) {
				if (!player.getInventory().addItem(item)) {
					player.send(new MessagePacket("The other player did not have enough open inventory slots"));
					resetTrade();					
					return;
				}
				//tradingWith.getInventory().removeItem(item);
			}
			player.send(new InventoryPacket());
			player.send(new CloseTradePacket());
			tradingWith.send(new InventoryPacket());
			tradingWith.send(new CloseTradePacket());
			player.getTrade().setOfferAccepted(false);
			player.getTrade().setConfirmAccepted(false);
			player.getTradeContainer().getItems().clear();
			tradingWith.getTrade().setOfferAccepted(false);
			tradingWith.getTrade().setConfirmAccepted(false);
			tradingWith.getTradeContainer().getItems().clear();
			player.getTrade().inventoryCopy.clear();
			tradingWith.getTrade().inventoryCopy.clear();
			tradingWith.getTrade().setTradingWith(null);
			tradingWith.setGameAction(GameAction.NOTHING);
			player.getTrade().setTradingWith(null);
			player.setGameAction(GameAction.NOTHING);
		}
	}
	public void resetTrade() {
		if (tradingWith != null) {
			if (!player.getTrade().inventoryCopy.isEmpty()) {
				player.getInventory().getItems().clear();
				player.getInventory().getItems().addAll(player.getTrade().inventoryCopy);
				player.getTrade().inventoryCopy.clear();
			}
			if (!tradingWith.getTrade().inventoryCopy.isEmpty()) {
				tradingWith.getInventory().getItems().clear();
				tradingWith.getInventory().getItems().addAll(tradingWith.getTrade().inventoryCopy);
				tradingWith.getTrade().inventoryCopy.clear();
			}
			tradingWith.getTradeContainer().getItems().clear();
			tradingWith.getTrade().setTradingWith(null);
			tradingWith.setGameAction(GameAction.NOTHING);
			tradingWith.send(new CloseTradePacket());
			player.send(new MessagePacket("You declined the trade"));
			tradingWith.send(new MessagePacket("The other player declined the trade."));
			tradingWith.send(new InventoryPacket());
			tradingWith.getTrade().setOfferAccepted(false);
			tradingWith.getTrade().setConfirmAccepted(false);
		}
		player.getTrade().setOfferAccepted(false);
		player.getTrade().setConfirmAccepted(false);
		player.getTradeContainer().getItems().clear();
		player.send(new CloseTradePacket());
		player.setGameAction(GameAction.NOTHING);
		player.send(new InventoryPacket());
		tradingWith = null;
	}

	public Player getTradingWith() {
		return tradingWith;
	}

	public void setTradingWith(Player tradingWith) {
		this.tradingWith = tradingWith;
	}
	
	public ArrayList<GameItem> createOldInventory(Player player) {
		ArrayList<GameItem> copy = new ArrayList<GameItem>(32);
		for (GameItem item : player.getInventory().getItems()) {
			copy.add(new GameItem(item));
		}
		return copy;
	}

	public boolean isOfferAccepted() {
		return offerAccepted;
	}

	public void setOfferAccepted(boolean offerAccepted) {
		this.offerAccepted = offerAccepted;
	}

	public boolean isConfirmAccepted() {
		return confirmAccepted;
	}

	public void setConfirmAccepted(boolean confirmAccepted) {
		this.confirmAccepted = confirmAccepted;
	}
	
}

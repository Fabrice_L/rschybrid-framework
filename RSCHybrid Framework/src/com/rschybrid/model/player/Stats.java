package com.rschybrid.model.player;

import java.util.HashMap;

import com.rschybrid.net.packet.outgoing.MessagePacket;
import com.rschybrid.net.packet.outgoing.UpdateStat;

public class Stats {
	
	private Player player;
	
	public Stats (Player player) {
		this.player = player;
		for (Stat stat : Stat.values()) {
			if (stat == Stat.HITPOINTS) {
				setStat(stat, 10);
			} else {
				setStat(stat, 1);
			}
		}
	}
	
	private HashMap<Stat, int[]> stats = new HashMap<Stat, int[]>(18);
	
	public void setStat(Stat stat, int level) {
		getStats().put(stat, new int[]{getXpForLevel(level), level});
	}
	
	public int getExperience(Stat stat) {
		return getStats().get(stat)[0];
	}
	
	public void setExperience(Stat stat, int experience) {
		getStats().get(stat)[0] = experience;
	}
	
	public int getLevel(Stat stat) {
		return getStats().get(stat)[1];
	}
	
	public void setLevel(Stat stat, int level) {
		getStats().get(stat)[1] = level;
	}
	
	public int getMaxLevel(Stat stat) {
		return getLevelForXp(getExperience(stat));
	}
	
	public boolean addExperience(Stat stat, int experience) {
		int currentXp = getExperience(stat);
		int currentLevel = getLevel(stat);
		int maxLevel = getLevelForXp(currentXp);
		int levelDifference = maxLevel - currentLevel;
		if (experience + currentXp >= 200000000) {
			player.send(new MessagePacket("EXP Limit Reached!"));
			return false;
		}
		getStats().get(stat)[0] += experience;
		int newLevel = getLevelForXp(getStats().get(stat)[0]) - levelDifference;
		getStats().get(stat)[1] = newLevel;
		
		if (maxLevel < getLevelForXp(getStats().get(stat)[0])) {
			int levelsAdded = getLevelForXp(getStats().get(stat)[0]) - maxLevel;	
			player.send(new MessagePacket("@gre@You have advanced " + levelsAdded + " " + stat.toString().toLowerCase() + " level" + (levelsAdded > 1 ? "s" : "") + "!"));
		}
		player.send(new UpdateStat(stat));
		player.setOurAppearanceChanged(true);
		return true;
	}
	
	public static int getXpForLevel(int level) {
		int points = 0;
		int output = 0;

		for (int lvl = 1; lvl <= level; lvl++) {
			points += Math.floor((double) lvl + 300.0
					* Math.pow(2.0, (double) lvl / 7.0));
			if (lvl >= level)
				return output;
			output = (int) Math.floor(points / 4);
		}
		return 0;
	}

   public static int getLevelForXp(int exp) {
        int points = 0;
        int output = 0;
        for (int lvl = 1; lvl < 100; lvl++) {
            points += Math.floor((double)lvl + 300.0 * Math.pow(2.0, (double)lvl / 7.0));
            output = (int)Math.floor(points / 4);
            if ((output - 1) >= exp) {
                return lvl;
            }
        }
        return 99;
    }
   
	public int getCombatLevel() {
		double attack = getMaxLevel(Stat.ATTACK) + getMaxLevel(Stat.STRENGTH);
		double defense = getMaxLevel(Stat.DEFENSE) + getMaxLevel(Stat.HITPOINTS);
		double mage = getMaxLevel(Stat.PRAYER) + getMaxLevel(Stat.MAGIC);
		mage /= 8D;

		if(attack < ((double)getMaxLevel(Stat.RANGED) * 1.5D)) {
			return (int)((defense / 4D) + ((double)getMaxLevel(Stat.RANGED) * 0.375D) + mage);
		}
		else {
			return (int)((attack / 4D) + (defense / 4D) + mage);
		}
	}
	
	public HashMap<Stat, int[]> getStats() {
		return stats;
	}

	public enum Stat {
		ATTACK,
		DEFENSE,
		STRENGTH,
		HITPOINTS,
		RANGED,
		PRAYER,
		MAGIC,
		COOKING,
		WOODCUTTING,
		FLETCHING,
		FISHING,
		FIREMAKING,
		CRAFTING,
		SMITHING,
		MINING,
		HERBLAW,
		AGILITY,
		THIEVING;
	}
}

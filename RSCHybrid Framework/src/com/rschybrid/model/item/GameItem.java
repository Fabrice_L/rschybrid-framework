package com.rschybrid.model.item;

import com.rschybrid.model.Entity;
import com.rschybrid.model.item.def.ItemDef;
import com.rschybrid.model.item.def.ItemWieldableDef;

public class GameItem extends Entity {
	
	private int amount;
	private boolean wielded;
	
	public GameItem(GameItem item) {
		this.setId(item.getId());
		this.amount = item.getAmount();
		this.wielded = item.isWielded();
	}
	

	public GameItem(int id, int amount) {
		this.setId(id);
		this.amount = amount;
	}

	public int getAmount() {
		return amount;
	}

	public void setAmount(int amount) {
		this.amount = amount;
	}
	
	public int addAmount(int amount) {
		return this.amount += amount;
	}
	
	public int removeAmount(int amount) {
		return this.amount -= amount;
	}
	
	public ItemDef getDefinitions() {
		return ItemDef.getDef(getId());
	}
	
	public ItemWieldableDef getWieldDefinitions() {
		return ItemWieldableDef.getDef(getId());
	}

	public boolean isWielded() {
		return wielded;
	}

	public void setWielded(boolean wielded) {
		this.wielded = wielded;
	}
}

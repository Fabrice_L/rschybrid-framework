package com.rschybrid.model.item;

import com.rschybrid.model.Entity;
import com.rschybrid.model.Point;
import com.rschybrid.model.item.def.ItemDef;
import com.rschybrid.model.player.Player;

public class GroundItem extends Entity{

	private Player owner;
	private int amount;
	private long spawnedTime;
	private int privateTime = 180;
	private int publicTime = 240;
	private ItemState state = ItemState.PUBLIC;
	
	public GroundItem(GameItem item, Player owner, Point location) {
		this.setId(item.getId());
		this.setAmount(item.getAmount());
		this.setOwner(owner);
		this.setSpawnedTime(System.currentTimeMillis());
		this.setLocation(location);
	}
	
	public boolean canMerge(GroundItem item) {
		if (ItemDef.getDef(item.getId()).isStackable()) {
			return true;
		}
		return false;
	}

	public int getAmount() {
		return amount;
	}

	public void setAmount(int amount) {
		this.amount = amount;
	}

	public long getSpawnedTime() {
		return spawnedTime;
	}

	public void setSpawnedTime(long spawnedTime) {
		this.spawnedTime = spawnedTime;
	}

	public Player getOwner() {
		return owner;
	}

	public void setOwner(Player owner) {
		this.owner = owner;
	}

	public int getPrivateTime() {
		return privateTime;
	}
	
	public void decreasePrivateTime() {
		privateTime--;
	}
	
	public int getPublicTime() {
		return publicTime;
	}
	
	public void decreasePublicTime() {
		publicTime--;
	}
	
	public boolean visibleTo(Player player) {
		switch(getState()) {
		case HIDDEN:
			return false;
		case PRIVATE:
			if (player == owner) {
				return true;
			}
			return false;
		case PUBLIC:
			return true;
		}
		return false;
	}
	
	public ItemState getState() {
		return state;
	}

	public void setState(ItemState state) {
		this.state = state;
	}

	public enum ItemState {
		PRIVATE,
		HIDDEN,
		PUBLIC;
	}
}

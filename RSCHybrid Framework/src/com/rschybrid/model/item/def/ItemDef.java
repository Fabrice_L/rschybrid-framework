package com.rschybrid.model.item.def;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.logging.Level;

import com.rschybrid.model.EntityDef;
import com.rschybrid.util.sql.MySQL;

/**
 * The definition wrapper for items
 */
public class ItemDef extends EntityDef {
	
	private static ArrayList<ItemDef> items = new ArrayList<ItemDef>();

	/**
	 * The command of the object
	 */
	public String command;
	/**
	 * The base price of the object
	 */
	public int basePrice;
	/**
	 * The sprite id
	 */
	public int sprite;
	/**
	 * Whether the item is stackable or not
	 */
	public boolean stackable;
	/**
	 * Whether the item is wieldable or not
	 */
	public boolean wieldable;
	/**
	 * PictureMask
	 */
	public int pictureMask;

	public String getCommand() {
		return command;
	}
	
	public int getSprite() {
		return sprite;
	}

	public int getBasePrice() {
		return basePrice;
	}

	public boolean isStackable() {
		return stackable;
	}

	public boolean isWieldable() {
		return wieldable;
	}
	
	public int getPictureMask() {
		return pictureMask;
	}
	
	public static void load() {
		try {
			MySQL.open();
			String loadTableSQL = "SELECT * FROM item_def"; 
			
			PreparedStatement load = MySQL.connection.prepareStatement(loadTableSQL);
			ResultSet result = load.executeQuery();
			
			while(result.next()) {
				ItemDef item = new ItemDef();
				item.name = result.getString("name");
				item.description = result.getString("description");
				item.command = result.getString("command");
				item.sprite = result.getInt("sprite");
				item.basePrice = result.getInt("basePrice");
				item.stackable = result.getBoolean("stackable");
				item.wieldable = result.getBoolean("wieldable");
				item.pictureMask = result.getInt("pictureMask");
				items.add(item);
			}
            result.close();
            MySQL.close();
            logger.log(Level.INFO, "Loaded " + items.size() + " item definitions.");
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	
	public static ItemDef getDef(int id) {
		if(id < 0 || id >= items.size()) {
			return null;
		}
		return items.get(id);
	}
}

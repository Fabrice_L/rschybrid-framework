package com.rschybrid.model.item.def;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map.Entry;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.rschybrid.util.sql.MySQL;

/**
 * The definition wrapper for items
 */
public class ItemWieldableDef {
	
	protected static final Logger logger = Logger.getLogger(ItemWieldableDef.class.getName());

	
	private static HashMap<Integer, ItemWieldableDef> itemWieldable = new HashMap<Integer, ItemWieldableDef>();
	private static HashMap<Integer, int[]> itemAffectedTypes = new HashMap<Integer, int[]>();


	/**
	 * Sprite Id of the item
	 */
	private int sprite;
	/**
	 * Type of item
	 */
	private int type;
	/**
	 * Body position the item is drawn on
	 */
	private int wieldPos;
	/**
	 * How many armour points should be given
	 */
	private int armourPoints;
	/**
	 * How many weapon aim points should be given
	 */
	private int weaponAimPoints;
	/**
	 * How many weapon power points should be given
	 */
	private int weaponPowerPoints;
	/**
	 * How many magic points should be given
	 */
	private int magicPoints;
	/**
	 * How many prayer points should be given
	 */
	private int prayerPoints;
	/**
	 * How many range points should be given
	 */
	private int rangePoints;
	/**
	 * The level of each stat required to wield
	 */
	private HashMap<Integer, Integer> requiredStats;
	/**
	 * If the item can only be worn by females (used for female plates)
	 */
	private boolean femaleOnly;
	
	public int getSprite() {
		return sprite;
	}
	
	public int getType() {
		return type;
	}
	
	public int[] getAffectedTypes() {
		int[] affectedTypes = itemAffectedTypes.get(type);
		if(affectedTypes != null) {
			return affectedTypes;
		}
		return new int[0];
	}
	
	public int getWieldPos() {
		return wieldPos;
	}
	
	public int getArmourPoints() {
		return armourPoints;
	}
	
	public int getWeaponAimPoints() {
		return weaponAimPoints;
	}
	
	public int getWeaponPowerPoints() {
		return weaponPowerPoints;
	}
	
	public int getMagicPoints() {
		return magicPoints;
	}
	
	public int getPrayerPoints() {
		return prayerPoints;
	}
	
	public int getRangePoints() {
		return rangePoints;
	}
	
	public Set<Entry<Integer, Integer>> getStatsRequired() {
		return requiredStats.entrySet();
	}
	
	public boolean femaleOnly() {
		return femaleOnly;
	}
	
	public static void load() {
		try {
			MySQL.open();
			String loadTableSQL = "SELECT * FROM item_wieldable_def"; 	
			PreparedStatement load = MySQL.connection.prepareStatement(loadTableSQL);
			ResultSet result = load.executeQuery();
			
			while(result.next()) {
				ItemWieldableDef item = new ItemWieldableDef();
				int id = result.getInt("id");
				item.sprite = result.getInt("sprite");
				item.type = result.getInt("type");
				item.wieldPos = result.getInt("wieldPos");
				item.armourPoints = result.getInt("armourPoints");
				item.weaponAimPoints = result.getInt("weaponAimPoints");
				item.weaponPowerPoints = result.getInt("weaponPowerPoints");
				item.magicPoints = result.getInt("magicPoints");
				item.prayerPoints = result.getInt("prayerPoints");
				item.rangePoints = result.getInt("rangePoints");
				HashMap<Integer, Integer> statsRequired = new HashMap<Integer, Integer>();
				String[] reqStatArray = result.getString("requiredStats").split(" ");    
				for ( String ss : reqStatArray) {
					if (!ss.equals("")) {
						String [] intArray = ss.split("-");
						statsRequired.put(Integer.parseInt(intArray[0]), Integer.parseInt(intArray[1]));
					}
				}
				item.requiredStats = statsRequired;
				item.femaleOnly = result.getBoolean("femaleOnly");
				itemWieldable.put(id, item);

			}
			
            logger.log(Level.INFO, "Loaded " + itemWieldable.size() + " wieldable item definitions.");
			
			loadTableSQL = "SELECT * FROM item_wield_affected"; 
			load = MySQL.connection.prepareStatement(loadTableSQL);
			result = load.executeQuery();
			
			while(result.next()) {
				int type = result.getInt("type");
				String[] typesAffectedString = result.getString("affectedType").split(" ");
				int[] typesAffected = new int[typesAffectedString.length];
				for (int i = 0; i < typesAffectedString.length; i++) {
					typesAffected[i] = Integer.parseInt(typesAffectedString[i]);
				}
				itemAffectedTypes.put(type, typesAffected);

			}
			
			result.close();
			MySQL.close();
            logger.log(Level.INFO, "Loaded " + itemAffectedTypes.size() + " wield affected itemtype definitions.");
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	
	public static ItemWieldableDef getDef(int id) {
		if(id < 0) {
			return null;
		}
		return itemWieldable.get(id);
	}
}

package com.rschybrid.model;

public class ChatMessage {

	private Entity sender;
	private byte[] message;
	private Entity recipient;


	public ChatMessage(Entity sender, byte[] message) {
		this.sender = sender;
		this.message = message;
	}
	
	public ChatMessage(Entity sender, byte[] message, Entity recipient) {
		this.sender = sender;
		this.message = message;
		this.recipient = recipient;
	}

	public Entity getSender() {
		return sender;
	}
	
	public byte[] getMessage() {
		return message;
	}

	public Entity getRecipient() {
		return recipient;
	}


}
package com.rschybrid.model.landscape.def;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.rschybrid.util.sql.MySQL;

public class TileDef {
	
	protected static final Logger logger = Logger.getLogger(TileDef.class.getName());
	
	private static ArrayList<TileDef> tiles = new ArrayList<TileDef>();
	
	public int colour;
	public int unknown;
	public int objectType;
	
	public int getColour() {
		return colour;
	}
	
	public int getUnknown() {
		return unknown;
	}
	
	public int getObjectType() {
		return objectType;
	}
	
	public static void load() {
		try {
			MySQL.open();
			String loadTableSQL = "SELECT * FROM tile_def"; 
			
			PreparedStatement load = MySQL.connection.prepareStatement(loadTableSQL);
			ResultSet result = load.executeQuery();
			
			while(result.next()) {
				TileDef tile = new TileDef();
				tile.colour = result.getInt("colour");
				tile.unknown = result.getInt("unknown");
				tile.objectType = result.getInt("objectType");
				tiles.add(tile);
			}
            result.close();
            MySQL.close();
            logger.log(Level.INFO, "Loaded " + tiles.size() + " tile definitions.");
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	
	public static TileDef getDef(int id) {
		if(id < 0 || id >= tiles.size()) {
			return null;
		}
		return tiles.get(id);
	}
}
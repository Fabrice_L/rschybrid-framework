package com.rschybrid.model.object.def;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.logging.Level;

import com.rschybrid.model.EntityDef;
import com.rschybrid.util.sql.MySQL;

/**
 * The definition wrapper for doors
 */
public class DoorDef extends EntityDef {
	
	private static ArrayList<DoorDef> doors = new ArrayList<DoorDef>();

	/**
	 * The first command of the door
	 */
	public String command1;
	/**
	 * The second command of the door
	 */
	public String command2;
	/**
	 * The doors type.
	 */
	public int doorType;
	/**
	 * Unknown
	 */
	public int unknown;
	
	public int modelVar1;
	public int modelVar2;
	public int modelVar3;

	public String getCommand1() {
		return command1.toLowerCase();
	}

	public String getCommand2() {
		return command2.toLowerCase();
	}

	public int getDoorType() {
		return doorType;
	}
	
	public int getUnknown() {
		return unknown;
	}
	
	public int getModelVar1() {
		return modelVar1;
	}
	
	public int getModelVar2() {
		return modelVar2;
	}
	
	public int getModelVar3() {
		return modelVar3;
	}
	
	public static void load() {
		try {
			MySQL.open();
			String loadTableSQL = "SELECT * FROM door_def"; 
			
			PreparedStatement load = MySQL.connection.prepareStatement(loadTableSQL);
			ResultSet result = load.executeQuery();
			
			while(result.next()) {
				DoorDef door = new DoorDef();
				door.name = result.getString("name");
				door.description = result.getString("description");
				door.command1 = result.getString("command1");
				door.command2 = result.getString("command2");
				door.modelVar1 = result.getInt("modelVar1");
				door.modelVar2 = result.getInt("modelVar2");
				door.modelVar3 = result.getInt("modelVar3");
				door.doorType = result.getInt("doorType");
				door.unknown = result.getInt("unknown");
				doors.add(door);
			}
            result.close();
            MySQL.close();
            logger.log(Level.INFO, "Loaded " + doors.size() + " door definitions.");
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	
	public static DoorDef getDef(int id) {
		if(id < 0 || id >= doors.size()) {
			return null;
		}
		return doors.get(id);
	}
}

package com.rschybrid.model;

import com.rschybrid.RSCHybrid;
import com.rschybrid.model.pulse.Pulse;

public class Entity {
	
	private int[][] mobSprites = new int[][]{
		{3, 2, 1},
		{4, -1, 0},
		{5, 6, 7}
	};

	private int id;
	private String name;
	private Point location;
	private int sprite = 1;
	private boolean spriteChanged;
	private boolean ourAppearanceChanged = true;
	private int appearanceID = 0;
	private boolean hasMoved;
	
	private PathHandler pathHandler = new PathHandler(this);
	private Entity following;
	private Pulse followEvent;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public Point getLocation() {
		return location;
	}

	public void setLocation(Point location) {
		this.location = location;
	}
	
	public void setLocation(Point p, boolean teleported) {
		if (!teleported) {
			updateSprite(p);
			hasMoved = true;
		}
		setLocation(p);
	}
	
	protected void updateSprite(Point newLocation) {
		try {
			int xIndex = getLocation().getX() - newLocation.getX() + 1;
			int yIndex = getLocation().getY() - newLocation.getY() + 1;
			setSprite(mobSprites[xIndex][yIndex]);
		}
		catch(Exception e) {
			e.printStackTrace();
		}
	}

	public int getSprite() {
		return sprite;
	}

	public void setSprite(int sprite) {
		this.setSpriteChanged(true);
		this.sprite = sprite;
	}

	public boolean isSpriteChanged() {
		return spriteChanged;
	}

	public void setSpriteChanged(boolean spriteChanged) {
		this.spriteChanged = spriteChanged;
	}
	
	public void updateAppearanceID() {
		if(ourAppearanceChanged) {
			appearanceID++;
		}
	}
	
	public static byte[] getPositionOffsets(Point p1, Point p2) {
		byte[] rv = new byte[2];
		rv[0] = getCoordOffset(p1.getX(), p2.getX());
		rv[1] = getCoordOffset(p1.getY(), p2.getY());
		return rv;
	}
	
	private static byte getCoordOffset(int coord1, int coord2) {
		byte offset = (byte)(coord1 - coord2);
		if(offset < 0) {
			offset += 32;
		}
		return offset;
	}

	public boolean isOurAppearanceChanged() {
		return ourAppearanceChanged;
	}

	public void setOurAppearanceChanged(boolean ourAppearanceChanged) {
		this.ourAppearanceChanged = ourAppearanceChanged;
	}

	public int getAppearanceID() {
		return appearanceID;
	}

	public void setAppearanceID(int appearanceID) {
		this.appearanceID = appearanceID;
	}

	public boolean hasMoved() {
		return hasMoved;
	}

	public void setHasMoved(boolean hasMoved) {
		this.hasMoved = hasMoved;
	}

	public PathHandler getPathHandler() {
		return pathHandler;
	}

	public Entity getFollowing() {
		return following;
	}
	
	public boolean isFollowing() {
		return followEvent != null && following != null;
	}

	public boolean isFollowing(Entity entity) {
		return isFollowing() && entity.equals(following);
	}

	public void setFollowing(Entity entity) {
		setFollowing(entity, 0);
	}
	
	public void setFollowing(final Entity entity, final int radius) {
		if(isFollowing()) {
			resetFollowing();
		}
		following = entity;
		followEvent = new Pulse(0, "followEvent") {

			@Override
			public void execute() {
				Entity owner = getPulseOwner();
				if(!owner.getLocation().withinDistance(following.getLocation())) {
					resetFollowing();
				}
				else if(!owner.getPathHandler().finishedPath() && owner.getLocation().withinDistance(following.getLocation(), radius)) {
					owner.getPathHandler().resetPath();
				}
				else if(owner.getPathHandler().finishedPath() && !owner.getLocation().withinDistance(following.getLocation(), radius + 1)) {
					owner.getPathHandler().setPath(new Path(owner.getLocation().getX(), owner.getLocation().getY(), following.getLocation().getX(), following.getLocation().getY()));
				}
			}
			
		};
		followEvent.setPulseOwner(this);
		RSCHybrid.submit(followEvent);
	}

	public void resetFollowing() {
		following = null;
		if(followEvent != null) {
			followEvent.stop();
			followEvent = null;
		}
		pathHandler.resetPath();
	}

}

package com.rschybrid;

import java.util.Iterator;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.rschybrid.model.item.def.ItemDef;
import com.rschybrid.model.item.def.ItemWieldableDef;
import com.rschybrid.model.landscape.def.TileDef;
import com.rschybrid.model.object.def.DoorDef;
import com.rschybrid.model.player.Player;
import com.rschybrid.model.pulse.Pulse;
import com.rschybrid.model.pulse.PulseManager;
import com.rschybrid.net.ChannelHandler;
import com.rschybrid.net.codec.Decoder;
import com.rschybrid.util.webapi.RestAPI;
import com.rschybrid.world.ItemHandler;
import com.rschybrid.world.LandscapeHandler;
import com.rschybrid.world.PlayerHandler;

import io.netty.bootstrap.ServerBootstrap;
import io.netty.channel.ChannelFuture;
import io.netty.channel.ChannelInitializer;
import io.netty.channel.ChannelOption;
import io.netty.channel.EventLoopGroup;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.channel.socket.SocketChannel;
import io.netty.channel.socket.nio.NioServerSocketChannel;
import io.netty.handler.traffic.ChannelTrafficShapingHandler;

public class RSCHybrid implements Runnable {
	
	private static final Logger logger = Logger.getLogger(RSCHybrid.class.getName());
	
    private boolean running = true;
    private long lastPulse = 0L;
    
	private static PulseManager pulseManager = new PulseManager();

	public static void submit(final Pulse pulse) {
		pulseManager.submit(pulse);
	}
	
	public static PulseManager getPulseManager() {
		return pulseManager;
	}


	public static void main(String[] args) {
		ItemDef.load();
		ItemWieldableDef.load();
		DoorDef.load();
		TileDef.load();
		LandscapeHandler.load();
		new Thread(new RestAPI()).start();
		new Thread(new RSCHybrid()).start();
	}

	@Override
	public void run() {
        final int kbps = 25;
        final int bytesPerSecond = kbps * 125;
		EventLoopGroup bossGroup = new NioEventLoopGroup();
		EventLoopGroup workerGroup = new NioEventLoopGroup();
		try {
            ServerBootstrap b = new ServerBootstrap();
            b.group(bossGroup, workerGroup)
             .channel(NioServerSocketChannel.class)
             .childHandler(new ChannelInitializer<SocketChannel>() {
                 @Override
                 public void initChannel(SocketChannel ch) throws Exception {
                    ch.pipeline().addLast(new ChannelTrafficShapingHandler(bytesPerSecond, bytesPerSecond));
            		ch.pipeline().addLast("decoder", new Decoder());
            		ch.pipeline().addLast("handler", new ChannelHandler());
                 }
             })
             .option(ChannelOption.SO_BACKLOG, 128)  
             .childOption(ChannelOption.SO_KEEPALIVE, true); 

            ChannelFuture f = b.bind(Config.getServerPort()).sync(); 
          
            logger.log(Level.INFO, Config.getServerName() + " started on port: " + Config.getServerPort());
            gameLoop();
            
            f.channel().closeFuture().sync();
            
		} catch (InterruptedException e) {
			e.printStackTrace();
		} finally {
			workerGroup.shutdownGracefully();
            bossGroup.shutdownGracefully();
		}
	}
	
	private void gameLoop() {
        logger.log(Level.INFO, "RSCHybrid is now online");
		while (running) {
		    long stampDiff = (System.nanoTime() / 1000000) - lastPulse;
		    if (stampDiff < 600) {
		        try {
		            Thread.sleep(10); // Surrender this time slice
		        } catch (InterruptedException e) {
		            System.err.println("Error occurred while sleeping between game pulses");
		        }
		
		        continue;
		    }
		    
		    Iterator<Pulse> pulse = pulseManager.getPulses().iterator();
			while(pulse.hasNext()) {
				Pulse p = pulse.next();
				p.cycle();
				if(!p.isRunning()) {
					pulse.remove();
				}
			}
		    
		    for (int i = 0; i < PlayerHandler.getPlayerSessions().size(); i++) {
		    	Player player = PlayerHandler.getPlayerSessions().get(i);
		    	player.getPacketManager().processQueuedPackets();
		    }
		    
		    for (Player player : PlayerHandler.getPlayersOnline()) {
		    	PlayerHandler.updatePlayer(player);
		    	ItemHandler.updateItem(player);
		    }
		    
		    lastPulse = System.nanoTime() / 1000000;
		}
	}

	
}

package com.rschybrid;

public class Config {

	private static String serverName = "RSCHybrid";
	private static int serverPort = 5555;
	private static int gameVersion = 1;
	
	private static int maxPlayers = 500;

	public static String getServerName() {
		return serverName;
	}

	public static int getServerPort() {
		return serverPort;
	}

	public static int getMaxPlayers() {
		return maxPlayers;
	}

	public static int getGameVersion() {
		return gameVersion;
	}

}

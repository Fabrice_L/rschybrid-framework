package com.rschybrid.util;

import java.io.BufferedInputStream;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.util.Random;

import com.rschybrid.model.Point;

public class Misc {
	
    private static final Random rand = new Random();
	private static final int[] headSprites = {1,4,6,7,8};
	private static final int[] bodySprites = {2,5};
	
    private static char characters[] = {
            ' ', 'e', 't', 'a', 'o', 'i', 'h', 'n', 's', 'r',
            'd', 'l', 'u', 'm', 'w', 'c', 'y', 'f', 'g', 'p',
            'b', 'v', 'k', 'x', 'j', 'q', 'z', '0', '1', '2',
            '3', '4', '5', '6', '7', '8', '9', ' ', '!', '?',
            '.', ',', ':', ';', '(', ')', '-', '&', '*', '\\',
            '\'', '@', '#', '+', '=', '\243', '$', '%', '"', '[',
            ']'
    };
	
	public static long generateSessionKey(byte userByte) {
		return rand.nextLong();
	}
	
	public static int getHeight(int y) {
		return (int)(y / 944);
	}

	public static int getHeight(Point location) {
		return getHeight(location.getY());
	}
	
	public static boolean inIntArray(int number, int[] array) {
		for (int i : array) {
			if (i == number) {
				return true;
			}
		}
		return false;
	}

	public static long usernameToHash(String s) {
		s = s.toLowerCase();
		String s1 = "";
		for (int i = 0; i < s.length(); i++) {
			char c = s.charAt(i);
			if (c >= 'a' && c <= 'z')
				s1 = s1 + c;
			else if (c >= '0' && c <= '9')
				s1 = s1 + c;
			else
				s1 = s1 + ' ';
		}

		s1 = s1.trim();
		if (s1.length() > 12)
			s1 = s1.substring(0, 12);
		long l = 0L;
		for (int j = 0; j < s1.length(); j++) {
			char c1 = s1.charAt(j);
			l *= 37L;
			if (c1 >= 'a' && c1 <= 'z')
				l += (1 + c1) - 97;
			else if (c1 >= '0' && c1 <= '9')
				l += (27 + c1) - 48;
		}
		return l;
	}
	
	/**
	 * Converts a usernames hash back to the username
	 */
	public static String hashToUsername(long l) {
		if (l < 0L)
			return "invalid_name";
		String s = "";
		while (l != 0L) {
			int i = (int) (l % 37L);
			l /= 37L;
			if (i == 0)
				s = " " + s;
			else if (i < 27) {
				if (l % 37L == 0L)
					s = (char) ((i + 65) - 1) + s;
				else
					s = (char) ((i + 97) - 1) + s;
			} else {
				s = (char) ((i + 48) - 27) + s;
			}
		}
		return s;
	}
	
	public static String byteToString(byte[] data, int offset, int length) {
        char[] buffer = new char[100];
        try {
            int k = 0;
            int l = -1;
            for (int i1 = 0; i1 < length; i1++) {
                int j1 = data[offset++] & 0xff;
                int k1 = j1 >> 4 & 0xf;
                if (l == -1) {
                    if (k1 < 13) {
                        buffer[k++] = characters[k1];
                    } else {
                        l = k1;
                    }
                } else {
                    buffer[k++] = characters[((l << 4) + k1) - 195];
                    l = -1;
                }
                k1 = j1 & 0xf;
                if (l == -1) {
                    if (k1 < 13) {
                        buffer[k++] = characters[k1];
                    } else {
                        l = k1;
                    }
                } else {
                    buffer[k++] = characters[((l << 4) + k1) - 195];
                    l = -1;
                }
            }
            boolean flag = true;
            for (int l1 = 0; l1 < k; l1++) {
                char c = buffer[l1];
                if (l1 > 4 && c == '@') {
                    buffer[l1] = ' ';
                }
                if (c == '%') {
                    buffer[l1] = ' ';
                }
                if (flag && c >= 'a' && c <= 'z') {
                    buffer[l1] += '\uFFE0';
                    flag = false;
                }
                if (c == '.' || c == '!' || c == ':') {
                    flag = true;
                }
            }
            return new String(buffer, 0, k);
        }
        catch (Exception e) {
            return ".";
        }
    }
	
	public static final ByteBuffer streamToBuffer(BufferedInputStream in) throws IOException {
		byte[] buffer = new byte[in.available()];
		in.read(buffer, 0, buffer.length);
		return ByteBuffer.wrap(buffer);
	}

	public static int[] getHeadsprites() {
		return headSprites;
	}

	public static int[] getBodysprites() {
		return bodySprites;
	}
	
	public static byte[] getObjectPositionOffsets(Point p1, Point p2) {
		byte[] rv = new byte[2];
		rv[0] = getObjectCoordOffset(p1.getX(), p2.getX());
		rv[1] = getObjectCoordOffset(p1.getY(), p2.getY());
		return rv;
	}

	private static byte getObjectCoordOffset(int coord1, int coord2) {
		return (byte)(coord1 - coord2);
	}
}

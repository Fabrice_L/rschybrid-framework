package com.rschybrid.util.webapi.impl;

import java.util.List;
import java.util.Map;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.rschybrid.model.player.Player;
import com.rschybrid.model.player.Stats.Stat;
import com.rschybrid.util.webapi.RequestBase;
import com.rschybrid.world.PlayerHandler;
import com.sun.net.httpserver.HttpServer;

public class ServerInfo extends RequestBase {

    public ServerInfo(String context, HttpServer server) {
        super(context, server);
    }

    @Override
    protected String getResponseJson(Map<String, List<String>> requestParameters) {
        Gson gson = new GsonBuilder().setPrettyPrinting().create();

        JsonObject serverInfo = new JsonObject();
        serverInfo.addProperty("playersOnline", PlayerHandler.getPlayersOnline().size());

        JsonArray jsonPlayers = new JsonArray();

        for(Player player : PlayerHandler.getPlayersOnline()) {
            if(player != null) {
                JsonObject playerObject = new JsonObject();
                playerObject.addProperty("username", player.getName());
                for(Stat stat : Stat.values()) {
                	playerObject.addProperty(stat.toString().toLowerCase(), player.getStats().getLevel(stat));
                }

                jsonPlayers.add(playerObject);
            }
        }

        serverInfo.add("players", jsonPlayers);

        return gson.toJson(serverInfo);
    }
}
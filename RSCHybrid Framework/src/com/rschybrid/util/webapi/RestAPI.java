package com.rschybrid.util.webapi;

import java.net.InetSocketAddress;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.rschybrid.util.webapi.impl.ServerInfo;
import com.sun.net.httpserver.HttpServer;

public class RestAPI implements Runnable {
	
	private static final Logger logger = Logger.getLogger(RestAPI.class.getName());

    public static String hostname = "localhost";
    public static Integer port    = 8080;

    @Override
    public void run() {
        try {
            final HttpServer server = HttpServer.create(new InetSocketAddress(hostname, port), 1);

            //new PlayerInfo("/player/info", server);
            new ServerInfo("/server/info", server);

            server.start();
            logger.log(Level.INFO,"RESTful API started on port: " + port);
        } catch(Exception ex) {
            System.out.println(ex.getMessage());
        }
    }
}

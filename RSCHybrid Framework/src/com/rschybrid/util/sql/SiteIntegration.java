package com.rschybrid.util.sql;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

import com.rschybrid.model.player.Player;
import com.rschybrid.model.player.Rights;
import com.rschybrid.model.player.Stats.Stat;

public class SiteIntegration {
	
	public static int databaseLogin(Player player) {
		int code = 0;
		try {
		MySQL.open();
		String loadTableSQL = "SELECT * FROM users WHERE username='" + player.getName() + "'"; 
		
		PreparedStatement load = MySQL.connection.prepareStatement(loadTableSQL);
		ResultSet result = load.executeQuery();
		
		while(result.next()) {
			String hashedPass = passHash(player.getPassword(), result.getString("salt"));
			if (result.getString("username").equalsIgnoreCase(player.getName()) 
					&& result.getString("password").equals(hashedPass)) {
				player.setRights(Rights.values()[result.getInt("rights")]);
				loadStats(player, result.getInt("id"));
				code = 1;
			}
		}
		result.close();
        MySQL.close();
		} catch(Exception e) {
			e.printStackTrace();
		}
		return code;
	}
	
    private static void loadStats(Player player, int id) {
		try {
		MySQL.open();
		String loadTableSQL = "SELECT * FROM user_stats WHERE id='" + id + "'"; 
		
		PreparedStatement load = MySQL.connection.prepareStatement(loadTableSQL);
		ResultSet result = load.executeQuery();
		
		while(result.next()) {
			for (Stat stat : Stat.values()) {
				player.getStats().setLevel(stat, result.getInt(stat.toString().toLowerCase()));
				player.getStats().setExperience(stat, result.getInt(stat.toString().toLowerCase() + "_xp"));
			}
		}
		result.close();
        MySQL.close();
		} catch(Exception e) {
			e.printStackTrace();
		}
	}

	public static String passHash(String in, String salt) {
        String salted = stringToMd5(salt);
        String b = stringToMd5(salted + in);
        return b;
    }

    private static String stringToMd5(String input) {
        String original = input;
        MessageDigest md;
        try {
            md = MessageDigest.getInstance("MD5");
            md.update(original.getBytes());
            byte messageDigest[] = md.digest();
            StringBuffer hexString = new StringBuffer();
            for (int i=0;i<messageDigest.length;i++) {
                String hex=Integer.toHexString(0xFF & messageDigest[i]);
                if (hex.length() == 1) {
                    hexString.append('0');
                }
                hexString.append(hex);
            }
            return hexString.toString();
        } catch (NoSuchAlgorithmException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return null;
    }
}

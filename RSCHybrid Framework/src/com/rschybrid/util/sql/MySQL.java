package com.rschybrid.util.sql;

import java.sql.Connection;
import java.sql.DriverManager;

public class MySQL {
	
	public static Connection connection = null;
	public static Connection forumConnection = null;
		
	public synchronized static void open()
	{
		try {
			connection = (Connection) DriverManager.getConnection("jdbc:mysql://localhost:3306/rschybrid", "root", "");
		} catch(Exception e) {
			e.printStackTrace();
		}
	}
	
	/*public synchronized static void openForum()
	{
		try {
			forumConnection = (Connection) DriverManager.getConnection("jdbc:mysql://" + Config.ForumDBHost + ":3306/" + Config.ForumDBName + "?zeroDateTimeBehavior=convertToNull", Config.ForumDBUser, Config.ForumDBPass);
		} catch(Exception e) {
			e.printStackTrace();
		}
	}*/
	
	public synchronized static void close()
	{
		try {
			connection.close();
			//forumConnection.close();
		} catch(Exception e) {
			e.printStackTrace();
		}
	}
}